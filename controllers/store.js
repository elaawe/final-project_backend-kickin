const {
    Store,
    User,
    Field
} = require('../models')
const imagekit = require('../lib/imagekit');
const response = require('../helpers/responseFormater');;


module.exports = {

    async update(req, res) {
        let {
            store_name,
            phone_number,
            address,
            bank_name,
            account_name,
            account_number,
            facilities
        } = req.body


        try {
            let isAuthorized = await Store.findOne({
                where: {
                    user_id: req.user.id
                }
            })
            let user = await User.findOne({
                where: {
                    id: req.user.id
                }
            })
            if (!isAuthorized) {
                throw new Error("You need to create your own Store!")
            } else {
                if (req.file === undefined) {
                    let store = await Store.update({
                        store_name,
                        phone_number,
                        address,
                        bank_name,
                        account_name,
                        account_number,
                        facilities
                    }, {
                        where: {
                            user_id: req.user.id
                        }
                    })
                    user = user.entity
                    res.status(200).json(
                        response.success({
                            user,
                            store_name,
                            phone_number,
                            address,
                            bank_name,
                            account_name,
                            account_number,
                            facilities,
                        })
                    )
                } else {
                    const split = req.file.originalname.split('.');
                    const ext = split[split.length - 1];
                    var image = await imagekit.upload({
                        file: req.file.buffer,
                        fileName: `IMG-${Date.now()}.${ext}`
                    })

                    let store = await Store.update({
                        store_name,
                        avatar: image.url,
                        phone_number,
                        address,
                        bank_name,
                        account_name,
                        account_number,
                        facilities
                    }, {
                        where: {
                            user_id: req.user.id
                        }
                    })
                    user = user.entity

                    res.status(200).json(
                        response.success({
                            user,
                            store_name,
                            phone_number,
                            address,
                            bank_name,
                            account_name,
                            account_number,
                            facilities,
                            avatar: image.url
                        })
                    )
                }
            }
        } catch (err) {
            res.status(401).json(
                response.error(err)
            )
        }
    },
    async getStoreInfo(req, res) {
        try {
            let storeInfo = await Store.findOne({
                where: {
                    user_id: req.user.id
                }
            })
            res.status(200).json(
                response.success(storeInfo)
            )
        } catch (err) {
            res.status(410).json(
                response.error(new Error('Cannot find any Store'))
            )
        }
    },

    async getAllStore(req, res) {
        let count = await Store.count()
        let page = await Math.floor(count / 12) + 1
        try {
            let findStore = await Store.findAll({
                include: {
                    model: Field
                },
                limit: 12,
                offset: (req.query.page - 1) * 12,
                order: [
                    ['store_name', 'ASC']
                ]
            })
            res.status(200).json(
                response.success({count, page, findStore})
            )
        } catch (err) {
            res.status(410).json(
                response.error(new Error('Cannot find any Store'))
            )
        }
    },
    async deleteStore(req, res) {
        try {
            let before = await Store.findOne({
                where: {
                    id: req.params.id
                }
            })
            let isSuperAdmin = await User.findOne({
                where: {
                    id: req.user.id
                }
            })
            if (isSuperAdmin.id !== 1) {
                res.status(403).json(
                    response.error(new Error("Only Super Admin can do this!"))
                )
            } else {
                let deleted = await Store.destroy({
                    where: {
                        id: req.params.id
                    }
                })

                res.status(200).json(
                    response.success(`${before.store_name} Successfuly deleted!`)
                )
            }
        } catch (err) {
            res.status(410).json(
                response.error(new Error('Cannot Find the Store'))
            )
        }
    }
}