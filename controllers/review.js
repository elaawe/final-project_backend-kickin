const {
    Review,
    Profile,
    Field,
    User
} = require('../models')
const response = require('../helpers/responseFormater')
const db = require('../models');
const field = require('./field');

exports.createReview = async function (req, res, next) {
    try {
        let reviewExist = await Review.findOne({
            where: {
                user_id: req.user.id,
                field_id: req.params.id
            }
        })

        if (reviewExist) {
            throw new Error('You already reviewed this field!')
        } else {
            let review = await Review.create({
                comment: req.body.comment,
                rating: req.body.rating,
                user_id: req.user.id,
                field_id: req.params.id
            });

            let result = await Review.findAndCountAll({
                where: {
                    field_id: req.params.id
                }
            })
            let nganu = []
            result.rows.forEach(i => {
                nganu.push(
                    i.rating
                )
            })
            let sembarang = Number(nganu.reduce((a, b) => a + b, 0) / result.count).toFixed(1)

            let push = await Field.update({
                avg_rating: sembarang
            }, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json(
                response.success(sembarang)
            )
        }
    } catch (err) {
        res.status(422).json(
            response.error(err)
        )
    };

}

exports.updateReview = async function (req, res, next) {

    try {
        let {
            rating,
            comment
        } = req.body

        let dataUpdate = {
            rating,
            comment
        }

        await Review.update(dataUpdate, {
            where: {
                id: req.params.id
            }
        });
        res.status(200);
        res.data = "Successfully updated!";
        next();
    } catch (err) {
        res.status(422);
        next(err);
    };
}

exports.deleteReview = async function (req, res, next) {
    try {
        await Review.destroy({
            where: {
                id: req.params.id
            }
        });
        res.status(200);
        res.data = "Successfully deleted!";
        next();
    } catch (err) {
        res.status(500);
        next(err);
    };
}

exports.showRatingOverall = async function (req, res, next) {
    try {
        let reviews = await Review.findAll({
            where: {
                field_id: req.params.id
            },
            attributes: ['id', 'rating', 'comment', 'field_id', 'user_id'],
        })
        if (reviews) {
            let n = 0
            let sum = 0
            var overall = 0

            reviews.forEach(el => {
                n += 1
                sum += el.rating
            });

            overall = sum / n
        } else {
            res.status(404);
            next(err);
        }
        res.status(200);
        res.data = {
            overall
        };
        next()
    } catch (err) {
        res.status(404);
        next(err);
    }
}

exports.findAllReview = async function (req, res, next) {
    try {
        let count = await Review.count()
        let page = await Math.ceil(count / 10)
        let reviews = await Review.findAll({
            where: {
                field_id: req.params.id
            },
            order: [
                ['id', 'ASC']
            ],
            attributes: ['id', 'rating', 'comment', 'field_id', 'user_id'],
            limit: 10,
            offset: (req.query.page - 1) * 10,
            include: [
                {
                    model: User,
                    attributes: ['id'],
                    include: {
                        model: Profile
                    }
                }
            ]
        })
        if (reviews) {
            let n = 0
            let sum = 0
            var overall = 0

            reviews.forEach(el => {
                n += 1
                sum += el.rating
            });

            overallRating = sum / n
        } else {
            res.status(404);
            next(err);
        }
        res.status(200);
        res.data = { count, page, reviews };
        next()
    } catch (err) {
        res.status(404);
        next(err);
    }
}

exports.findUserReview = async function (req, res, next) {
    try {
        let count = await Review.count({
            where: {
                user_id: req.user.id
            }
        })
        let page = await Math.floor(count / 10) + 1
        let reviews = await Review.findAll({
            where: {
                user_id: req.user.id
            },
            order: [
                ['id', 'ASC']
            ],
            attributes: ['id', 'rating', 'comment', 'field_id', 'user_id'],
            limit: 10,
            offset: (req.query.page - 1) * 10,
            include: [
                {
                    model: Field
                }
            ]
        })
        res.status(200);
        res.data = { count, page, reviews };
        next()
    } catch (err) {
        res.status(404);
        next(err);
    }
}