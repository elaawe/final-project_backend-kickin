const {
    Field,
    Image_field,
    Review,
    Profile,
    Store,
    User
} = require('../models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const generate = require('../helpers/fieldCode')
const imagekit = require('../lib/imagekit');
const response = require('../helpers/responseFormater');
const db = require('../models');


module.exports = {
    async createField(req, res, next) {
        let {
            name,
            field_type,
            price
        } = req.body
        try {
            var image = await Promise.all(
                req.files.map(i =>
                    imagekit.upload({
                        file: i.buffer,
                        fileName: `IMG-${Date.now()}.${i.originalname.split('.')[i.originalname.split('.').length - 1]}}`
                    })
                )
            )
            let store = await Store.findOne({
                where: {
                    user_id: req.user.id
                }
            })
            let field = await Field.create({
                name,
                field_type,
                price,
                store_id: store.id,
                user_id: req.user.id,
                avg_rating: 0
            })
            let fieldCode = await generate.generateFieldCode(field.id)

            let fieldCodeGenerate = await Field.update({
                field_code: fieldCode
            }, {
                where: {
                    id: field.id
                }
            })

            // for multiple image upload
            let url_image = await image.map(img => img.url)
            let imageField = null
            url_image.forEach(url => {
                imageField = Image_field.create({
                    image_field_url: url,
                    field_id: field.id
                })
            });
            let result = await Field.findOne({
                where: {
                    id: field.id
                }
            })
            let imageF = await Image_field.findAll({
                where: {
                    field_id: result.id
                }
            })
            res.status(201).json(
                response.success({ result, imageF })
            )

        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },


    async update(req, res) {
        let {
            name,
            field_type,
            price
        } = req.body
        try {
            var image = await Promise.all(
                req.files.map(i =>
                    imagekit.upload({
                        file: i.buffer,
                        fileName: `IMG-${Date.now()}.${i.originalname.split('.')[i.originalname.split('.').length - 1]}}`
                    })
                )
            )
            let isAuthorized = await Field.findOne({
                where: {
                    user_id: req.user.id
                }
            })
            if (!isAuthorized) {
                throw new Error("You need to create your own Field!")
            } else {
                var image = await Promise.all(
                    req.files.map(i =>
                        imagekit.upload({
                            file: i.buffer,
                            fileName: `IMG-${Date.now()}.${i.originalname.split('.')[i.originalname.split('.').length - 1]}}`
                        })
                    )
                )
                let coba = await Field.update({
                    name,
                    field_type,
                    price
                }, {
                    where: {
                        id: req.params.id
                    }
                })
                let field = await Field.findOne({
                    where: {
                        id: req.params.id
                    }
                })
                // for multiple image upload
                let url_image = await image.map(img => img.url)
                let imageField = null
                url_image.forEach(url => {
                    imageField = Image_field.create({
                        image_field_url: url,
                        field_id: field.id
                    })
                });
                let imageF = await Image_field.findAll({
                    where: {
                        field_id: req.params.id
                    }
                })

                res.status(200).json(
                    response.success({ field, imageF })
                )
            }
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },
    async showFieldMainPage(req, res, next) {
        try {
            let count = await Field.count()
            let fields = await Field.findAll({
                attributes: {
                    exclude: ['created_at', 'updated_at']
                },
                include: [
                    {
                        model: Store,
                        as: 'storeInfo', attributes: ['id', 'store_name', 'address', 'phone_number', 'facilities', 'avatar', 'user_id']
                    },
                    { model: Image_field }],
                limit: 4,
                order: [
                    ['updated_at', 'DESC'],
                ]
            })
            res.status(200);
            res.data = { count, fields }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async showField(req, res, next) {
        try {
            let count = await Field.count()
            let page = await Math.floor(count / 12) + 1
            let fields = await Field.findAll({
                include: [
                    {
                        model: Store,
                        as: 'storeInfo', attributes: ['id', 'store_name', 'address', 'phone_number', 'facilities', 'avatar', 'user_id']
                    },
                    { model: Image_field }],
                limit: 12,
                offset: (req.query.page - 1) * 12,
                order: [
                    ['name', 'ASC'],
                ]
            })
            res.status(200);
            res.data = {
                count,
                page,
                fields

            }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },
    async showFieldbyStore(req, res, next) {
        try {
            let count = await Field.count()
            let page = await Math.floor(count / 12) + 1
            let fields = await Field.findAll({
                include: [
                    {
                        model: Store,
                        as: 'storeInfo',
                        attributes: ['id', 'store_name', 'address', 'phone_number', 'facilities', 'avatar', 'user_id'],
                        where: {
                            user_id: req.user.id
                        }
                    },
                    { model: Image_field }],
                limit: 12,
                offset: (req.query.page - 1) * 12,
                order: [
                    ['name', 'ASC'],
                ]
            })
            res.status(200);
            res.data = {
                count,
                page,
                fields

            }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async showFieldSortbyPriceAsc(req, res, next) {
        try {
            let count = await Field.count()
            let page = await Math.floor(count / 12) + 1
            let fields = await Field.findAll({
                include: [
                    { model: Store, as: 'storeInfo', attributes: ['id', 'store_name', 'address', 'phone_number', 'facilities', 'avatar', 'user_id'] },
                    { model: Image_field }],
                limit: 12,
                offset: (req.query.page - 1) * 12,
                order: [
                    ['price', 'ASC'],
                ]
            })
            res.status(200);
            res.data = {
                count,
                page,
                fields
            }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async showFieldSortbyPriceDesc(req, res, next) {
        try {
            let count = await Field.count()
            let page = await Math.floor(count / 12) + 1
            let fields = await Field.findAll({
                include: [
                    { model: Store, as: 'storeInfo', attributes: ['id', 'store_name', 'address', 'phone_number', 'facilities', 'avatar', 'user_id'] },
                    { model: Image_field }],
                limit: 12,
                offset: (req.query.page - 1) * 12,
                order: [
                    ['price', 'DESC'],
                ]
            })
            res.status(200);
            res.data = {
                count,
                page,
                fields
            }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async showFieldSortbyNameAsc(req, res, next) {
        try {
            let count = await Field.count()
            let page = await Math.floor(count / 12) + 1
            let fields = await Field.findAll({
                include: [
                    {
                        model: Store,
                        as: 'storeInfo'
                    },
                    { model: Image_field }],
                order: [
                    [
                        { model: Store, as: 'storeInfo' },
                        'store_name', 'ASC'
                    ]
                ],
                limit: 12,
                offset: (req.query.page - 1) * 12,
            })
            res.status(200);
            res.data = {
                count,
                page,
                fields
            }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async showFieldSortbyNameDesc(req, res, next) {
        try {
            let count = await Field.count()
            let page = await Math.floor(count / 12) + 1
            let fields = await Field.findAll({
                include: [
                    {
                        model: Store,
                        as: 'storeInfo'
                    },
                    { model: Image_field }],
                order: [
                    [
                        { model: Store, as: 'storeInfo' },
                        'store_name', 'DESC'
                    ]
                ],
                limit: 12,
                offset: (req.query.page - 1) * 12
            })
            res.status(200);
            res.data = {
                count,
                page,
                fields
            }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async showFiledbyTypeField(req, res, next) {
        try {
            let count = await Field.count({
                where: {
                    field_type: `${req.query.field_type}`
                }
            })
            let page = await Math.floor(count / 12) + 1
            let filterField = await Field.findAll({
                where: {
                    field_type: `${req.query.field_type}`
                },
                attributes: {
                    exclude: ['updated_at', 'created_at']
                },
                include: [
                    { model: Store, as: 'storeInfo', attributes: ['id', 'store_name', 'avatar', 'user_id'] },
                    { model: Image_field }],
                limit: 12,
                offset: (req.query.page - 1) * 12,
                order: [
                    ['name', 'ASC'],
                ]
            })
            res.status(200);
            res.data = { count, page, filterField }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async deleteField(req, res, next) {
        try {
            let isAuthorized = await Field.findOne({
                where: {
                    user_id: req.user.id
                }
            })
            if (!isAuthorized) {
                res.status(410).json(
                    response.error(new Error("You are not allowed to do this!")
                    ))
            } else {
                let field = await Field.destroy({
                    where: {
                        id: req.params.id
                    },
                })
                res.status(200).json(
                    response.success(`${isAuthorized.name} Successfuly Deleted!`)
                )
            }

        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },

    async showOneField(req, res, next) {
        try {
            let field = await Field.findOne({
                where: {
                    id: req.params.id
                },
                attributes: ['id', 'name', 'field_type', 'price', 'avg_rating'],
                include: [
                    { model: Image_field },
                    { model: Store, as: 'storeInfo', },
                    { model: Review, include: { model: User, include: { model: Profile } } }
                ]
            })

            res.status(200);
            res.data = {
                field
            }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async searchField(req, res, next) {
        try {

            let count = await Field.count({
                include: [
                    {
                        model: Store, as: 'storeInfo', where: {
                            store_name: {
                                [Op.iLike]: `%${req.query.store_name}%`
                            }
                        }
                    }
                ]
            })
            let page = await Math.floor(count / 12) + 1
            let fields = await Field.findAll({
                include: [
                    {
                        model: Store, as: 'storeInfo', where: {
                            store_name: {
                                [Op.iLike]: `%${req.query.store_name}%`
                            }
                        }
                    },
                    { model: Image_field }
                ],
                limit: 12,
                offset: (req.query.page - 1) * 12,
                order: [
                    ['name', 'ASC'],
                ]
            })

            res.status(200);
            res.data = {
                count,
                page,
                fields
            }
            next()
        } catch (err) {
            res.status(422)
            next(err)
        }
    },
    async sortByRatingDESC(req, res) {
        try {
            let count = await Field.count()
            let page = await Math.floor(count / 12) + 1
            let result = await Field.findAll({
                attributes: ['id', 'name', 'price', 'avg_rating'],
                limit: 12,
                offset: (req.query.page - 1) * 12,
                order: [['avg_rating', 'DESC']],
                include: [
                    { model: Store, as: 'storeInfo', },
                    { model: Image_field }
                ]

            })
            return res.status(200).json(response.success({ count, page, result }))

        } catch (err) {
            return res.status(422).json(response.error(err))
        }
    },
    async sortByRatingASC(req, res) {
        try {
            let count = await Field.count()
            let page = await Math.floor(count / 12) + 1
            let result = await Field.findAll({
                attributes: ['id', 'name', 'price', 'avg_rating'],
                limit: 12,
                offset: (req.query.page - 1) * 12,
                order: [['avg_rating', 'ASC']],
                include: [
                    { model: Store, as: 'storeInfo', },
                    { model: Image_field }
                ]

            })
            return res.status(200).json(response.success({ count, page, result }))

        } catch (err) {
            return res.status(422).json(response.error(err))
        }
    }
}