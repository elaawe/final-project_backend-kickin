const {
    Store,
    User,
    Profile,
    Review,
    Booking,
    Schedule
} = require('../models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const bcrypt = require('../helpers/bcrypt')
const jwt = require('../helpers/jwt')
const response = require('../helpers/responseFormater')


module.exports = {

    async register(req, res) {
        let {
            email,
            password,
            store_name
        } = req.body
        try {
            let isEmailAvail = await User.findOne({
                where: {
                    email
                }
            })
            let isStroeAvail = await Store.findOne({
                where: {
                    store_name
                }
            })
            if (isEmailAvail || isStroeAvail) {
                res.status(403).json(
                    response.error(new Error("Email or Store already resgitered!"))
                )
            } else {
                let user = await User.create({
                    email,
                    password,
                    role: "admin"
                })
                let store = await Store.create({
                    user_id: user.id,
                    store_name
                })
                let token = jwt.sign({
                    id: user.id
                })
                user = user.entity
                res.status(201).json(
                    response.success({
                        user,
                        store,
                        token
                    })
                )
            }
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )

        }
    },

    async adminLogin(req, res, next) {
        let {
            email,
            password
        } = req.body

        try {
            let admin = await User.findOne({
                where: {
                    email
                },
                attributes: ['id', 'email', 'password']
            })
            if (admin) {
                let isValidate = bcrypt.checker(password, admin.password)
                if (isValidate) {
                    let token = jwt.sign({
                        id: admin.id,
                        email: admin.email,
                        role: admin.role
                    })
                    admin = admin.entity
                    res.status(200).json(
                        response.success({
                            admin,
                            token
                        })
                    )
                } else {
                    res.status(401).json(
                        response.error(new Error("Wrong password!"))
                    )
                }
            } else {
                res.status(403).json(
                    response.error(new Error("Wrong Email!"))
                )
            }

        } catch (err) {
            res.status(401).json(
                response.error(err)
            )
        }
    },
    async adminUpdate(req, res, next) {
        let {
            email,
            role
        } = req.body

        try {
            let user = await User.update({
                email,
                role
            }, {
                where: {
                    id: req.params.id
                }
            })
            if (user) {
                res.status(200);
                res.data = "Successfully updated!";
                next();
            } else {
                throw new Error('Update is fail')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async adminDelete(req, res, next) {
        try {

            await Schedule.update({
                availability: true,
                user_id: null
            }, {
                where: {
                    user_id: req.params.id
                }
            })

            await Review.destroy({
                where: {
                    user_id: req.params.id
                }
            })

            await Profile.destroy({
                where: {
                    user_id: req.params.id
                }
            })


            await Booking.destroy({
                    where: {
                        user_id: req.params.id
                    }
                }),



                await Schedule.update({
                    availability: true,
                    user_id: null
                }, {
                    where: {
                        user_id: req.params.id
                    }
                })

            let user = await User.destroy({
                where: {
                    id: req.params.id
                }
            })
            if (user) {
                res.status(200);
                res.data = "Successfully deleted!";
                next();
            } else {
                throw new Error('Delete is fail')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async adminFindOne(req, res, next) {
        try {
            let user = await User.findOne({
                where: {
                    id: req.params.id
                }
            })
            if (user) {
                res.status(200);
                res.data = user;
                next();
            } else {
                throw new Error('User is not exist')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async adminFindAll(req, res, next) {
        try {
            let user = await User.findAll({
                limit: 10,
                offset: (req.query.page - 1) * 10,
                order: [
                    ['email', 'ASC'],
                ],
                attributes: ['id', 'email']
            })
            if (user) {
                res.status(200);
                res.data = user;
                next();
            } else {
                throw new Error('User is not exist')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async adminSearchUser(req, res, next) {
        try {
            let user = await User.findAll({
                where: {
                    email: {
                        [Op.iLike]: `%${req.body.email}%`
                    }
                },
                limit: 10,
                offset: (req.query.page - 1) * 10,
                order: [
                    ['email', 'ASC'],
                ],
                attributes: ['id', 'email']
            })
            if (user) {
                res.status(200);
                res.data = user;
                next();
            } else {
                throw new Error('User is not exist')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },


    check(req, res) {
        res.status(200).json(
            response.success({
                user: req.user
            })
        )
    }
}