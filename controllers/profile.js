const {
    Profile,
    User
} = require('../models')
const imagekit = require('../lib/imagekit');
const response = require('../helpers/responseFormater');


module.exports = {
    async update(req, res) {
        let {
            name,
            motto,
            age,
            favorite_team,
            favorite_position
        } = req.body
        try {
            if (req.file === undefined) {

                let profile = await Profile.update({
                    name,
                    motto,
                    age,
                    favorite_position,
                    favorite_team
                }, {
                    where: {
                        user_id: req.user.id
                    }
                })
                let user = await User.findOne({
                    where:{
                        id: req.user.id
                    },
                    include: {
                        model: Profile
                    }
                })
                user = user.entity
                let userProfile = await user.Profile
                res.status(201).json(
                    response.success({
                        user,
                        userProfile
                    })
                )

            } else {
                const split = req.file.originalname.split('.');
                const ext = split[split.length - 1];
                var image = await imagekit.upload({
                    file: req.file.buffer,
                    fileName: `IMG-${Date.now()}.${ext}`
                })
                let profile = await Profile.update({
                    name,
                    age,
                    motto,
                    avatar: image.url,
                    favorite_position,
                    favorite_team
                }, {
                    where: {
                        user_id: req.user.id
                    }
                })
                let user = await User.findOne({
                    where: {
                        id: req.user.id
                    },
                    include: {
                        model: Profile
                    }
                })
                user = user.entity
                let userProfile = await user.Profile
                res.status(201).json(
                    response.success({
                        user,
                        userProfile
                    })
                )
            }
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },

    async showProfile(req, res) {
        try {

            let profile = await Profile.findOne({
                where: {
                    user_id: req.user.id
                },
                attributes: {
                    exclude: ['created_at', 'updated_at']
                }
            })
            res.status(201).json(
                response.success({
                    profile
                })
            )
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    }
}