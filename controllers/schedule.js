const { Schedule, Field } = require('../models');
const { forEach } = require('../middlewares/exception');
const Sequelize = require('sequelize')
const Op = Sequelize.Op


exports.create = async function (req, res, next) {
    try {
        let {
            date,
            schedule_time
        } = req.body
        let field = await Field.findOne({
            where: {
                id: req.params.id,
            }
        })
        let scheduleAvailable = await Schedule.findOne({
            where: {
                date,
                schedule_time,
                field_id: field.id
            }
        })

        if (!scheduleAvailable) {
            var schedule = await Schedule.create({
                date,
                schedule_time,
                field_id: field.id
            });
        } else {
            throw new Error('Schedule has already exist!')
        }
        res.status(200);
        res.data = {
            schedule
        };
        next();
    } catch (err) {
        res.status(422);
        next(err)
    }
}

exports.showSchedules = async function (req, res, next) {
    try {
        const limiterDate = (now) => {
            let nowAday = `${now.getFullYear()}-0${now.getMonth() + 1}-${now.getDate()}`
            return nowAday
        }
        const limiterTime = (now) => {
            let hour = String(now.getHours())         
            if (hour.length === 1) {
                `0${hour}`
            } else {
                hour
            }

            let minute = String(now.getMinutes())
            if (minute.length === 1) {
                `0${minute}`
            } else {
                minute
            }

            let rightNow = `${hour}.${minute}-${Number(hour) + 1}.${minute}`
            return rightNow
        }
        let updateSchedules = await Schedule.update({
            availability: false
        }, {
            where: {
                [Op.and]: [
                    {
                        schedule_time: {
                            [Op.lt]: await limiterTime(new Date())
                        }
                    },
                    {
                        date: {
                            [Op.lte]: await limiterDate(new Date())
                        }
                    }
                ]
            }
        })

        let schedulesUpdated = await Schedule.findAll({
            where: {
                field_id: req.params.id,
                date: `${req.query.date}`
            },
            order: [
                ['schedule_time', 'ASC']
            ]
        })

        res.status(200);
        res.data = { schedulesUpdated };
        next();
    } catch (err) {
        res.status(400);
        next(err);
    }
}

exports.showAllSchedules = async function (req, res, next) {
    try {
        const limiterDate = (now) => {
            let nowAday = `${now.getFullYear()}-0${now.getMonth() + 1}-${now.getDate()}`
            return nowAday
        }

        const limiterTime = (now) => {
            let hour = String(now.getHours())

            if (hour.length === 1) {
                `0${hour}`
            } else {
                hour
            }

            let minute = String(now.getMinutes())
            if (minute.length === 1) {
                `0${minute}`
            } else {
                minute
            }

            let rightNow = `${hour}.${minute}-${Number(hour) + 1}.${minute}`
            return rightNow
        }
        let updateSchedules = await Schedule.update({
            availability: false
        }, {
            where: {
                [Op.and]: [
                    {
                        schedule_time: {
                            [Op.lt]: await limiterTime(new Date())
                        }
                    },
                    {
                        date: {
                            [Op.lte]: await limiterDate(new Date())
                        }
                    }
                ]
            }
        })
        let count = await Schedule.count({
            where: {
                field_id: req.params.id
            }
        })
        let schedulesUpdated = await Schedule.findAll({
            where: {
                field_id: req.params.id
            },
            attributes: {
                exclude: ['schedule_time', 'field_id', 'created_at', 'updated_at']
            },
            order: [
                ['date', 'DESC'],
                ['schedule_time', 'ASC']
            ]
        })

        res.status(200);
        res.data = { count, schedulesUpdated };
        next();
    } catch (err) {
        res.status(400);
        next(err);
    }
}

exports.updateSchedulesAvailable = async function (req, res, next) {
    try {
        let updatedSchedule = await Schedule.update({
            availability: true,
        }, {
            where: { id: req.params.id }
        });

        res.status(200);
        res.data = { updatedSchedule };
        next();
    } catch (err) {
        res.status(500);
        next(err);
    };
}

exports.updateSchedulesUnavailable = async function (req, res, next) {
    try {
        let updatedSchedule = await Schedule.update({
            availability: false,
        }, {
            where: { id: req.params.id }
        });
        res.status(200);
        res.data = { updatedSchedule };
        next();
    } catch (err) {
        res.status(500);
        next(err);
    };
}


exports.deleteSchedule = async function (req, res, next) {
    try {
        await Schedule.destroy({
            where: { id: req.params.id }
        });
        res.status(200);
        res.data = "Successfully deleted!";
        next();
    } catch (err) {
        res.status(500);
        next(err);
    };
}