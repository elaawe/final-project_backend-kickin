const { User, Profile, Review, Favorite, Booking, History_booking, Schedule } = require('../models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const bcrypt = require('../helpers/bcrypt')
const jwt = require('../helpers/jwt')
const {
    OAuth2Client
} = require('google-auth-library');
const axios = require('axios')
const passRandom = require('../helpers/passRandom')
const client = new OAuth2Client(process.env.CLIENT);
const response = require('../helpers/responseFormater')
const {registerConfirmation} = require('../helpers/Mail')


module.exports = {
    async register(req, res) {
        let {
            email,
            password,
            name
        } = req.body
        try {
            let user = await User.create({
                email,
                password
            })
            let profile = await Profile.create({
                user_id: user.id,
                name
            })
            let token = jwt.sign({
                id: user.id
            })
            user = user.entity
            res.status(201).json(
                response.success({
                    user,   
                    profile,
                    token
                })
            )
        } catch (err) {
            res.status(422).json(
                response.error(err.errors[0])
            )
         
        }
    },

    async login(req, res) {
        let {
            email,
            password
        } = req.body
        try {
            let user = await User.findOne({
                where: {
                    email
                }
            })
            let profile = await Profile.findOne({
                where: {
                    user_id: user.id
                }
            })
            if (user && user.role !== 'admin') {
                let isValidated = bcrypt.checker(password, user.password);

                if (isValidated) {
                    let token = jwt.sign({
                        id: user.id,
                        email: user.email,
                        role: user.role
                    })
                    user = user.entity
                    res.status(200).json(
                        response.success({
                            user,
                            profile,
                            token
                        })
                    )
                } else { 
                    res.status(401).json(
                        response.error(new Error("Wrong Password"))
                    )
                }

            } else {
                res.status(403).json(
                    response.error(new Error("You are not allowed to do this"))
                )
            }
        } catch {
            res.status(422).json(
                response.error(new Error("Email not registered yet"))
            )

        }
    },
    async googleLogin(req, res) {
        try {
          let passwordRandom = await passRandom()
          let access_token = req.body.access_token
          let google = await axios.post(`https://oauth2.googleapis.com/tokeninfo?id_token=${access_token}`)
          let result, profile, token
          // find user with email
    
          let user = await User.findOne({ where: { email: google.data.email } })
          if (user) {
            profile = await Profile.findOne({
               where:{ user_id: user.id}
            })
            user = user.entity
            token = await jwt.sign({
              id: user.id,
              email: user.email
            }, process.env.SECRET) 
            result = {user, profile, token}

          } else {
            user = await User.create({
                email: google.data.email,
                password: passwordRandom
            })
            profile = await Profile.create({
                user_id: user.id,
                name: req.body.name
            })
            user = user.entity
            let profile = await Profile.findOne({
                where: {
                    user_id: req.user.id
                }
            })
            token = await jwt.sign({
              id: user.id,
              email: user.email
            }, process.env.SECRET)
            const maildata = { email: user.email, name: profile.name, password: passwordRandom }
            await registerConfirmation(maildata)
            result = { user, profile, token }
            
            
          }
          res.status(200).json(
              response.success(result)
              )
        } catch (err) {
          res.status(401).json(
              response.error(err)
          )
        }
      },

      check(req, res) {
        res.status(200).json(
            response.success({
                user: req.user
            })
        )
    }
    

    // async googleLogin(req, res, next) {
    //     client.verifyIdToken({
    //         idToken: req.body.id_token,
    //         audience: process.env.CLIENT, // Specify the CLIENT_ID of the app that accesses the backend
    //         // Or, if multiple clients access the backend:
    //         //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    //     }).then(ticket => {
    //         const payload = ticket.getPayload();
    //         User.findOne({
    //                 where: {
    //                     email: payload.email
    //                 }
    //             })
    //             .then(data => {
    //                 if (data) {
    //                     return data
    //                 } else {
    //                     let obj = {
    //                         email: payload.email,
    //                         password: "apapun",
    //                         role: 'member'  
    //                     }
    //                     return User.create(obj)
    //                 }
    //             })
    //             .then(data => {
    //                 if (data) {
    //                     var access_token = jwt.jwtSign(data)
    //                 }
    //                 res.status(200).json({
    //                     access_token
    //                 })
    //             })
    //             .catch(err => {
    //                 next(err)
    //             })
    //     })
    // },

    
}