const {
    Store,
    Booking,
    Field,
    Payment,
    Schedule,
    User,
    Profile
} = require('../models')
const response = require('../helpers/responseFormater')
const imagekit = require('../lib/imagekit');
const Sequelize = require('sequelize')
const field = require('./field')
const {
    bookingCodeMail
} = require('../helpers/Mail');
const store = require('./store');
const Op = Sequelize.Op

module.exports = {

    async createBooking(req, res, next) {
        try {

            let isScheduleAvailable = await Schedule.findOne({
                where: {
                    date: req.body.date,
                    schedule_time: req.body.schedule_time,
                    field_id: req.params.id
                }
            })
            if (isScheduleAvailable.availability == false) {
                throw new Error("Schedule is not available!")
            } else {
                var bookCode = await Booking.create({})

                var fieldCode = await Field.findOne({
                    where: {
                        id: req.params.id
                    },
                    attributes: ['id', 'field_code'],
                    raw: true
                })


                let year = String(new Date().getFullYear())
                let codeYear = year[2] + year[3]
                let month = String(new Date().getMonth() + 1)
                let codeMonth = ''
                if (month.length === 1) {
                    codeMonth = `0${month}`
                } else {
                    codeMonth = month
                }
                var generateCodeBooking = (id) => {
                    let idString = String(id)
                    let zero = 5 - idString.length
                    let zeroCode = '';
                    for (let i = 0; i < zero; i++) {
                        zeroCode += '0'
                    }
                    let resultBookingCode = `${zeroCode}${idString}`
                    return resultBookingCode
                }
                var codeBooking = await generateCodeBooking(bookCode.id)
                var theSchedule = await Schedule.findOne({
                    where: {
                        field_id: fieldCode.id,
                        schedule_time: req.body.schedule_time,
                        date: req.body.date
                    }
                })



                var bookCodeUpdate = await Booking.update({
                    booking_code: `${fieldCode.field_code}${codeYear}${codeMonth}${codeBooking}`,
                    user_id: req.user.id,
                    schedule_id: theSchedule.id,
                    field_id: fieldCode.id,
                    status: 'waiting',
                    schedule_date: theSchedule.date,
                    schedule_time: theSchedule.schedule_time
                }, {
                    where: {
                        id: bookCode.id
                    }
                })
                await Schedule.update({
                    availability: false,
                    user_id: req.user.id
                }, {
                    where: {
                        id: theSchedule.id
                    }

                })
                const bookDetails = await Booking.findOne({
                    where: {
                        id: bookCode.id
                    },
                    attributes: ['booking_code'],
                    include: [{
                        model: User,
                        attributes: ['id', 'email'],
                        include: {
                            model: Profile,
                            attributes: ['name']
                        }
                    },
                    {
                        model: Field,
                        attributes: ['id', 'name', 'field_type'],
                        include: {
                            model: Store, as: 'storeInfo',
                        }
                    },
                    {
                        model: Schedule,
                        attributes: ['id', 'schedule_time', 'date']
                    }
                    ]
                })
                res.status(201).json(
                    response.success(bookDetails)
                )
            }
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },

    async showAllCheckoutBooking(req, res, next) {
        try {
            let count = await Booking.count({
                where: {
                    user_id: req.user.id,
                    status: ['waiting', 'validating']
                }
            })

            let page = await Math.floor(count / 3) + 1

            let bookings = await Booking.findAll({
                attributes: ['id', 'booking_code', 'status', 'user_id', 'schedule_date', 'schedule_time', 'schedule_id', 'updated_at'],
                limit: 3,
                offset: (req.query.page - 1) * 3,
                order: [
                    ['updated_at', 'DESC'],
                ],
                where: {
                    user_id: req.user.id,
                    status: ['waiting', 'validating']
                },
                include: [
                    {
                        model: Field,
                        include: { model: Store, as: 'storeInfo', },
                        attributes: {
                            exclude: ['updated_at', 'created_at']
                        }
                    },
                    {
                        model: Schedule,
                        attributes: {
                            exclude: ['updated_at', 'created_at']
                        }
                    }
                ]
            })
            res.status(200).json(
                response.success({
                    count,
                    page,
                    bookings
                })
            )
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },
    async showAllCheckoutBookingRN(req, res, next) {
        try {
            let count = await Booking.count({
                where: {
                    user_id: req.user.id,
                    status: ['waiting', 'validating']
                }
            })

            let bookings = await Booking.findAll({
                attributes: ['id', 'booking_code', 'status', 'user_id', 'schedule_date', 'schedule_time', 'schedule_id', 'updated_at'],
                limit: 10,
                order: [
                    ['updated_at', 'DESC'],
                ],
                where: {
                    user_id: req.user.id,
                    status: ['waiting', 'validating']
                },
                include: [
                    {
                        model: Field,
                        include: { model: Store, as: 'storeInfo', },
                        attributes: {
                            exclude: ['updated_at', 'created_at']
                        }
                    },
                    {
                        model: Schedule,
                        attributes: {
                            exclude: ['updated_at', 'created_at']
                        }
                    }
                ]
            })
            res.status(200).json(
                response.success({
                    count,
                    bookings
                })
            )
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },
    async showCheckoutBooking(req, res, next) {
        try {
            let bookings = await Booking.findOne({
                attributes: ['id', 'booking_code', 'status', 'user_id', 'schedule_date', 'schedule_time', 'schedule_id', 'updated_at'],
                where: {
                    id: req.params.id
                },
                include: [{
                    model: Field,
                    include: { model: Store, as: 'storeInfo', },
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                },
                {
                    model: Schedule,
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                }
                ]
            })
            res.status(200).json(
                response.success({
                    bookings
                })
            )
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },

    async payBooking(req, res, next) {
        const split = req.file.originalname.split('.');
        const ext = split[split.length - 1];
        try {
            var image = await imagekit.upload({
                file: req.file.buffer,
                fileName: `IMG-${Date.now()}.${ext}`
            })
            let booked = await Booking.findOne({
                where: {
                    id: req.params.id,
                    status: 'waiting',
                }
            })
            let field = await Field.findOne({
                where: {
                    id: booked.field_id
                }
            })
            let store = await Store.findOne({
                where: {
                    id: field.store_id
                }
            })

            let uploadPayment = await Payment.create({
                image_payment_proof_url: image.url,
                booking_id: booked.id,
                user_id: req.user.id,
                store_id: store.id
            })
            let statusUpdated = await Booking.update({
                status: 'validating'
            }, {
                where: {
                    id: req.params.id
                }
            })
            let numberOfBooking = await Profile.findOne({
                where: {
                    user_id: req.user.id
                }
            })
            let numberBooking = numberOfBooking.booking_number
            let bookingNumber = await Profile.update({
                booking_number: numberBooking += 1
            }, {
                where: {
                    user_id: req.user.id
                }
            })
            res.status(201).json(
                response.success({
                    uploadPayment,
                    statusUpdated,
                    bookingNumber
                })
            )
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },
    async showBookingTransfer(req, res, next) {
        try {
            let store = await Store.findOne({
                where: {
                    user_id: req.user.id
                }
            })
            let count = await Payment.count({
                where: {
                    store_id: store.id,
                    verified: false
                }
            })
            let page = await Math.floor(count / 10) + 1
            let proof_payments = await Payment.findAll({
                attributes: ['id', 'image_payment_proof_url', 'verified', 'booking_id'],
                where: {
                    verified: false,
                    store_id: store.id
                },
                include: [{
                    model: Booking,
                    include: [{
                        model: Field,
                        include: { model: Store, as: 'storeInfo', },
                    },
                    {
                        model: Schedule
                    }
                    ]
                },
                {
                    model: User,
                    include: [{
                        model: Profile
                    }]
                }
                ],
                limit: 10,
                offset: (req.query.page - 1) * 10,
                order: [
                    ['updated_at', 'DESC']
                ]
            })
            res.status(200).json(
                response.success({
                    count,
                    page,
                    proof_payments
                })
            )
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )

        }
    },

    async verifyBooking(req, res, next) {
        try {
            let booking = await Booking.update({
                status: 'paid'
            }, {
                where: {
                    booking_code: req.body.booking_code
                }
            })

            let bookingData = await Booking.findOne({
                where: {
                    booking_code: req.body.booking_code
                }
            })

            let verified = await Payment.update({
                verified: 'true'
            }, {
                where: {
                    booking_id: bookingData.id
                }
            })
            const bookDetails = await Booking.findOne({
                where: {
                    id: bookingData.id
                },
                attributes: ['booking_code'],
                include: [{
                    model: User,
                    attributes: ['id', 'email'],
                    include: {
                        model: Profile,
                        attributes: ['name']
                    }
                },
                {
                    model: Field,
                    attributes: ['id', 'name', 'field_type'],
                    include: {
                        model: Store, as: 'storeInfo',
                    }
                },
                {
                    model: Schedule,
                    attributes: ['id', 'schedule_time', 'date']
                }
                ]
            })
            await bookingCodeMail(bookDetails)

            res.status(200).json(
                response.success({
                    booking,
                    verified
                })
            )
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },
    async showAllBooking(req, res, next) {
        try {
            let store = await Store.findOne({
                where: {
                    user_id: req.user.id
                },
                raw: true
            })
            let field = await Field.findAll({
                where: {
                    store_id: store.id
                },
                raw: true
            })
            const allFieldId = await field.map(x => x.id)

            let count = await Booking.count({
                where: {
                    field_id: allFieldId
                }
            })
            let page = await Math.floor(count / 12) + 1
            let bookings = await Booking.findAll({
                attributes: ['id', 'booking_code', 'status', 'user_id', 'schedule_date', 'schedule_time', 'schedule_id', 'updated_at'],
                limit: 12,
                where: {
                    field_id: allFieldId
                },
                include: [{
                    model: User,
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                },
                {
                    model: Field,
                    include: { model: Store, as: 'storeInfo', },
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                },
                {
                    model: Schedule,
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                }
                ],
                offset: (req.query.page - 1) * 12,
                order: [
                    ['updated_at', 'DESC'],
                ]
            })
            res.status(200).json(
                response.success({
                    count,
                    page,
                    bookings
                })
            )
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },
    async showBookingTicket(req, res, next) {
        const limiterDate = (now) => {
            let nowAday = `${now.getFullYear()}-0${now.getMonth() + 1}-${now.getDate()}`
            return nowAday
        }
        const limiterTime = (now) => {
            let hour = String(now.getHours())


            if (hour.length === 1) {
                `0${hour}`
            } else {
                hour
            }

            let minute = String(now.getMinutes())

            if (minute.length === 1) {
                `0${minute}`
            } else {
                minute
            }

            let rightNow = `${Number(hour)}.${minute}-${Number(hour) + 1}.${minute}`

            return rightNow
        }
        try {
            var bookings = await Booking.update({
                status: 'expired'
            }, {
                where: {
                    status: ['waiting', 'validating', 'paid'],
                    user_id: req.user.id,
                    schedule_date: {
                        [Op.lt]: await limiterDate(new Date())
                    }
                },
            })
            let count = await Booking.count({
                where: {
                    status: 'paid',
                    user_id: req.user.id
                }
            })
            let page = await Math.floor(count / 2) + 1
            let showUpdateBookings = await Booking.findAll({
                limit: 2,
                offset: (req.query.page - 1) * 2,
                order: [
                    ['updated_at', 'DESC'],
                ],
                where: {
                    status: 'paid',
                    user_id: req.user.id
                },
                include: [{
                    model: User,
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                },
                {
                    model: Field,
                    include: { model: Store, as: 'storeInfo', },
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                },
                {
                    model: Schedule,
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                }
                ],
            })
            res.status(200).json(
                response.success({
                    count,
                    page,
                    showUpdateBookings
                })
            )

        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },
    async showExpiredBooking(req, res, next) {
        // var today = new Date();
        // var date = today.getFullYear() + '-' + '0' + (today.getMonth() + 1) + '-' + today.getDate();
        // var time = today.getHours() + "." + today.getMinutes() + "-" + (today.getHours() + 1) + "." + (today.getMinutes() + 1);
        // var dateTime = date + ' ' + time;
        const limiterDate = (now) => {
            let nowAday = `${now.getFullYear()}-0${now.getMonth() + 1}-${now.getDate()}`
            return nowAday
        }
        const limiterTime = (now) => {
            let hour = String(now.getHours())


            if (hour.length === 1) {
                `0${hour}`
            } else {
                hour
            }

            let minute = String(now.getMinutes())

            if (minute.length === 1) {
                `0${minute}`
            } else {
                minute
            }

            let rightNow = `${hour}.${minute}-${Number(hour) + 1}.${minute}`
            return rightNow
        }
        try {
            var bookings = await Booking.update({
                status: 'expired'
            }, {
                where: {
                    status: ['waiting', 'validating', 'paid'],
                    user_id: req.user.id,
                    schedule_date: {
                        [Op.lt]: await limiterDate(new Date())
                    }
                },
            })
            let count = await Booking.count({
                where: {
                    status: 'expired',
                    user_id: req.user.id
                }
            })
            let page = await Math.floor(count / 3) + 1
            let showExpiredBooking = await Booking.findAll({
                limit: 3,
                offset: (req.query.page - 1) * 3,
                order: [
                    ['updated_at', 'DESC'],
                ],
                where: {
                    status: 'expired',
                    user_id: req.user.id
                },
                include: [{
                    model: User,
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                },
                {
                    model: Field,
                    include: { model: Store, as: 'storeInfo', },
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                },
                {
                    model: Schedule,
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                }
                ],
            })
            res.status(200).json(
                response.success({
                    count,
                    page,
                    showExpiredBooking
                })
            )

        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },
    async showBookingList(req, res, next) {
        // var today = new Date();
        // var date = today.getFullYear() + '-' + '0' + (today.getMonth() + 1) + '-' + today.getDate();
        // var time = today.getHours() + "." + today.getMinutes() + "-" + (today.getHours() + 1) + "." + (today.getMinutes() + 1);
        const limiterDate = (now) => {
            let nowAday = `${now.getFullYear()}-0${now.getMonth() + 1}-${now.getDate()}`
            return nowAday
        }
        const limiterTime = (now) => {
            let hour = String(now.getHours())


            if (hour.length === 1) {
                `0${hour}`
            } else {
                hour
            }

            let minute = String(now.getMinutes())

            if (minute.length === 1) {
                `0${minute}`
            } else {
                minute
            }

            let rightNow = `${Number(hour)}.${minute}-${Number(hour) + 1}.${minute}`

            return rightNow
        }
        try {
            var bookings = await Booking.update({
                status: 'expired'
            }, {
                where: {
                    status: ['waiting', 'validating', 'paid'],
                    user_id: req.user.id,
                    schedule_date: {
                        [Op.lt]: await limiterDate(new Date())
                    }
                },
            })

            let count = await Booking.count({
                where: {
                    status: ['paid', 'expired'],
                    user_id: req.user.id
                }
            })
            let page = await Math.floor(count / 5) + 1
            let showBookingList = await Booking.findAll({
                limit: 5,
                offset: (req.query.page - 1) * 5,
                order: [
                    ['updated_at', 'DESC'],
                ],
                where: {
                    status: ['paid', 'expired'],
                    user_id: req.user.id
                },
                include: [{
                    model: User,
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                },
                {
                    model: Field,
                    include: { model: Store, as: 'storeInfo', },
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                },
                {
                    model: Schedule,
                    attributes: {
                        exclude: ['updated_at', 'created_at']
                    }
                }
                ],
            })
            res.status(200).json(
                response.success({
                    showBookingList
                })
            )

        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
        }
    },
    async cancelBooking(req, res, next) {
        try {
            let books = await Booking.findOne({
                where: {
                    id: req.params.id
                }
            })
            await Booking.destroy({
                where: {
                    id: books.id
                }
            })
            await Schedule.update({
                availability: true,
                user_id: null
            }, {
                where: {
                    id: books.schedule_id
                }
            })
            res.status(200).json(
                response.success("Successfully canceled")
            )
            next();
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
            next(err);
        }
    },
    async deletePayment(req, res, next) {
        try {
            let proof = await Payment.findOne({
                where: {
                    id: req.params.id
                }
            })
            await Payment.destroy({
                where: {
                    id: proof.id
                }
            })
            await Booking.update({
                status: "waiting",
                user_id: null
            }, {
                where: {
                    id: proof.booking_id
                }
            })
            res.status(200).json(
                response.success("Successfully canceled")
            )
            next();
        } catch (err) {
            res.status(422).json(
                response.error(err)
            )
            next(err);
        }
    }


}