const { Favorite } = require('../models')
module.exports = (req, res, next) => {

    let id = {
        where: {
            id: req.params.id
        }
    }
    Favorite.findOne(id)
        .then(fav => {
            if (fav) {
                if (fav.user_id === req.user.id) {
                    next()
                } else {
                    res.status(401).json({ msg: "unauthorized" })
                }
            } else {
                res.status(404).json({ msg: "data is not found" })
            }
        }).catch(err => {
            res.status(500).json(err)
        })
}