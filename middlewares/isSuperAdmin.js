const response = require('../helpers/responseFormater')

module.exports = (req, res, next) => {
    if(req.user.id === "1" ) return next()

    res.status(403).json(
        response.error(new Error("You are not allowed to do this!"))
    ) 
}