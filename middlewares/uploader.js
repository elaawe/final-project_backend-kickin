const multer = require('multer');
const path = require('path')
const maxSize = 5000000;
const upload = multer({
    fileFilter: function (req, file, callback) {
        var ext = path.extname(file.originalname);
        if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            return callback(new Error('Only images are allowed'))
        }
        callback(null, true)
    },
    limits:
    {
        fileSize: maxSize
    }
});

module.exports = upload;