module.exports = () => {
    let result = ''
    let number = '0123456789'
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    let charactersLength = characters.length
    for (var i = 0; i < 10; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    result += number.charAt(Math.floor(Math.random() * number.length))
    return result
  }
  