const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SENDGRID_API_KEY); // this function for setting sendgrid api

module.exports = {
    bookingCodeMail (nganu) {
        const msg = {
            to: nganu.User.email,
            from: 'kickinapps888@gmail.com', // Official kickin Email
            subject: 'Kickers Book Code',
            html: `<img src="https://ik.imagekit.io/kickin/photo_2020-07-15_00-08-54_A51_8palsT.jpg" alt="Kickin Logo">
         <h1>Let's Play!</h1>
         <p>Hi ${nganu.User.Profile.name},</p>
         <p>Here is your booking code: <strong>${nganu.booking_code}</strong> </p>
         <p>Booking Details:
            <ul>
                <li>Field Name: ${nganu.Field.name}</li>
                <li>Date: ${nganu.Schedule.date}</li>
                <li>Time: ${nganu.Schedule.schedule_time}</li>
                <li>Field Type: ${nganu.Field.field_type}</li>
                <li>Address: ${nganu.Field.storeInfo.address}</li>
            </ul>
            for more information please visit <a href="https://kickinapp.herokuapp.com/">www.kickinapp.herokuapp.com</a> 
         </p>

         <h4>Enjoy The Game! </h4>`
        };
        try {
            return sgMail.send(msg)
        } catch (err) {
            console.log(err)
        }
    },
    registerConfirmation (nganu) {
        const msg = {
            to: nganu.email,
            from: 'kickinapps888@gmail.com', // Official kickin Email
            subject: 'Kickers Resgisration Information',
            html: `<img src="https://ik.imagekit.io/kickin/photo_2020-07-15_00-08-54_A51_8palsT.jpg" alt="Kickin Logo">
         <h1>Let's Play!</h1>
         <p>Hi ${nganu.name},</p>
         <p>You're registered using your google account</p>
         <p>This is your default password '${nganu.password}'</p>
         <p>You can use it for direct login or update your current password with the new password</p>
         <p>Let's start amazing journey with us!</p>
            for more information please visit <a href="https://kickinapp.herokuapp.com/">www.kickinapp.herokuapp.com</a> 
         </p>

         <h4>Enjoy The Game! </h4>`
        };
        try {
            return sgMail.send(msg)
        } catch (err) {
            console.log(err)
        }
    }
}

