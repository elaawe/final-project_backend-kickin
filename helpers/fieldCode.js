exports.generateFieldCode = async function (id) {
    let idString = String(id)
    let codeLength = 5
    let zero = ''
    for(let i = 0; i<(codeLength - idString.length); i++){
        zero += '0'
    }
    let result =`${zero}${idString}`
    
    return result
}