module.exports = {
    success: (data) => ({
        status: "success",
        data
    }),

    error: (err) => ({
        status: "fail",
        errors: err.message
    })

}