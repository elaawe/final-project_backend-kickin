'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Name must be filled with name'
        }
      }
    },
    avatar: {
      type: DataTypes.TEXT,
      defaultValue: null,
      validate: {
        isUrl: {
          msg: 'Image must be Filled by URL'
        }
      }
    },
    motto: {
      type: DataTypes.STRING,
      defaultValue: null,
      validate: {
        notEmpty: {
          msg: 'Motto must be filled'
        }
      }
    },
    age: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    favorite_team: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    favorite_position: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    booking_number: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    user_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        },
        notEmpty: {
          msg: "user_id can't be empty"
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Profiles',
    underscored: true
  });
  Profile.associate = function(models) {
    // associations can be defined here
    Profile.belongsTo(models.User, {
      sourceKey: 'user_id',
      foreignKey: 'id'
    })
  };
  return Profile;
};
