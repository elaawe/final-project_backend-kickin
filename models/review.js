'use strict';
module.exports = (sequelize, DataTypes) => {
  const Review = sequelize.define('Review', {
    comment:  { 
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Comment must be filled'
        }
      }
    },
    rating: {
      type: DataTypes.DOUBLE,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        },
        notEmpty: {
          msg: "user_id can't be empty"
        }
      }
    },
    user_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        },
        notEmpty: {
          msg: "field_id can't be empty"
        }
      }
    },
    field_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isNumeric: {
          msg: 'Must be filled by integer'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Reviews',
    underscored: true
  });
  Review.associate = function(models) {
    // associations can be defined here
    Review.belongsTo(models.User, {
      foreignKey: 'user_id'
    }),
    Review.belongsTo(models.Field, {
      foreignKey: 'field_id'
    })
  };
  return Review;
};