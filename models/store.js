'use strict';
module.exports = (sequelize, DataTypes) => {
  const Store = sequelize.define('Store', {
    store_name: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
      validate: {
        notEmpty: {
          msg: 'Please define your store name'
        }
      }
    },
    avatar: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
    },
    phone_number: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null
    },
    facilities: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null
    },
    bank_name: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    account_name: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    account_number: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    }
  }, {
    sequelize,
    tableName: 'Stores',
    underscored: true
  });
  Store.associate = function (models) {
    // associations can be defined here
    Store.hasMany(models.Field, {
      foreignKey: 'store_id'
    })
    Store.belongsTo(models.User, {
      foreignKey: 'user_id'
    })
    Store.hasMany(models.Payment,{
      foreignKey: 'store_id'
    })
  };
  return Store;
};