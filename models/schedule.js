'use strict';
module.exports = (sequelize, DataTypes) => {
  const Schedule = sequelize.define('Schedule', {
    date: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isDate: {
          msg: 'Must be Filled by date'
        }
      }
    },
    schedule_time: {
      type: DataTypes.STRING,
      defaultValue: null,
      validate: {
        notEmpty: {
          msg: 'Motto must be filled'
        }
      }
    },
    availability: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        },
        notEmpty: {
          msg: "user_id can't be empty"
        }
      }
    },
    field_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isNumeric: {
          msg: 'Must be filled by integer'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Schedules',
    underscored: true
  });
  Schedule.associate = function (models) {
    // associations can be defined here
    Schedule.belongsTo(models.Field, {
      foreignKey: 'field_id'
    })
    Schedule.belongsTo(models.User, {
      foreignKey: 'user_id'
    }),
    Schedule.hasOne(models.Booking, {
      targetKey: 'id',
      foreignKey: 'schedule_id'
    })
  };
return Schedule;
};
