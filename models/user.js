'use strict';
const bcrypt = require('../helpers/bcrypt');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          msg: 'Must be filled with email'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Password must be filled'
        },
        len: [5, 20],
        isAlphanumeric: {
          msg: 'Password must be contain between alphabet and number'
        }
      }
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'member',
      validate: {
        notEmpty: {
          msg: 'Role must be filled'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Users',
    underscored: true,
    hooks: {
      beforeCreate: (user, options) => {
        user.password = bcrypt.hasher(user.password)
      }
    }
  });
  User.associate = function (models) {
    // associations can be defined here
    User.hasOne(models.Profile, {
      targetKey: 'id',
      foreignKey: 'user_id'
    })
    User.hasOne(models.Schedule, {
      targetKey: 'id',
      foreignKey: 'id'
    })
    User.hasMany(models.Review, {
      targetKey: 'id',
      foreignKey: 'id'
    })
    User.hasMany(models.Booking, {
      targetKey: 'id',
      foreignKey: 'id'
    })
    User.hasOne(models.Store, {
      foreignKey: 'user_id'
    })
    User.hasMany(models.Field, {
      foreignKey: 'user_id'
    })
    User.hasMany(models.Payment, {
      targetKey: 'id',
      foreignKey: 'user_id'
    })
  };

  // getter for public 
  Object.defineProperty(User.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
        role: this.role
      }
    }
  });

  return User;
};