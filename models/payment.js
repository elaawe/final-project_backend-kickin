'use strict';
module.exports = (sequelize, DataTypes) => {
  const Payment = sequelize.define('Payment', {
    image_payment_proof_url: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        isUrl: {
          msg: 'Image must be Filled by URL'
        }
      }
    },
    verified: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        },
        notEmpty: {
          msg: "user_id can't be empty"
        }
      }
    },
    store_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        },
        notEmpty: {
          msg: "store_id can't be empty"
        }
      }
    },
    booking_id: {
      type: DataTypes.INTEGER,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        },
        notEmpty: {
          msg: "booking_id can't be empty"
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Payments',
    underscored: true
  });
  Payment.associate = function(models) {
    // associations can be defined here
    Payment.belongsTo(models.Booking, {
      foreignKey: 'booking_id'
    })
    Payment.belongsTo(models.User, {
      foreignKey: 'user_id'
    })
    Payment.belongsTo(models.Store, {
      foreignKey: 'user_id'
    })
  };
  return Payment;
};