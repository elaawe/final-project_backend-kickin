'use strict';
module.exports = (sequelize, DataTypes) => {
  const Image_field = sequelize.define('Image_field', {
    image_field_url: {
      type: DataTypes.TEXT,
      defaultValue: null,
      validate: {
        isUrl: {
          msg: 'Image must be Filled by URL'
        }
      }
    },
    field_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isNumeric: {
          msg: 'Must be filled by integer'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Image_fields',
    underscored: true
  });
  Image_field.associate = function (models) {
    // associations can be defined here
    Image_field.belongsTo(models.Field, {
      foreignKey: 'field_id'
    })
  };
  return Image_field;
};