'use strict';
module.exports = (sequelize, DataTypes) => {
  const Field = sequelize.define('Field', {
    field_code: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
      validate: {
        notEmpty: {
          msg: 'Field Code must be filled'
        }
      }
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Name must be filled with name'
        }
      }
    },
    field_type: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Field type must be filled'
        }
      }
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaulValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        }
      }
    },
    avg_rating: {
      type: DataTypes.FLOAT,
      defaulValue: 0
    },
    store_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaulValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        }
      }
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaulValue: null,
      validate: {
        isNumeric: {
          msg: 'must be filled by integer'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Fields',
    underscored: true
  });
  Field.associate = function (models) {
    // associations can be defined here
    Field.hasMany(models.Schedule, {
      targetKey: 'id',
      foreignKey: 'field_id'
    })
    Field.hasMany(models.Image_field, {
      targetKey: 'id',
      foreignKey: 'field_id'
    }),
    Field.hasMany(models.Review, {
      targetKey: 'id',
      foreignKey: 'field_id'
    }),
    Field.hasMany(models.Booking, {
      targetKey: 'id',
      foreignKey: 'field_id'
    }),
    Field.belongsTo(models.Store, {
      foreignKey: 'store_id',
      as: 'storeInfo'
    }),
    Field.belongsTo(models.User, {
      foreignKey: 'user_id'
    })
  };
  return Field;
};