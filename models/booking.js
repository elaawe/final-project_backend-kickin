'use strict';
module.exports = (sequelize, DataTypes) => {
  const Booking = sequelize.define('Booking', {
    booking_code: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
      validate: {
        notEmpty: {
          msg: 'Booking must be filled'
        }
      }
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'waiting',
      validate: {
        notEmpty: {
          msg: 'Status must be filled'
        }
      }
    },
    schedule_date: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        notEmpty: {
          msg: 'Date must be filled'
        }
      }
    },
    schedule_time: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        notEmpty: {
          msg: 'Time must be filled'
        }
      }
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'Must be filled by integer'
        }
      }
    },
    schedule_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'Must be filled by integer'
        }
      }
    },
    field_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null,
      validate: {
        isNumeric: {
          msg: 'Must be filled by integer'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Bookings',
    underscored: true
  });
  Booking.associate = function (models) {
    // associations can be defined here
    Booking.belongsTo(models.Field, {
      foreignKey: 'field_id'
    })
    Booking.belongsTo(models.User, {
      foreignKey: 'user_id'
    })
    Booking.belongsTo(models.Schedule, {
      foreignKey: 'schedule_id'
    })
    Booking.hasMany(models.Payment, {
      targetKey: 'id',
      foreignKey: 'booking_id'
    })
  };
  return Booking;
};