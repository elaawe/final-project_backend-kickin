const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Field,
    Store
} = require('../models');
const jwt = require('jsonwebtoken');

let token;

describe('Test Field API Collection', () => {

    beforeAll(() => {
        // This whole code will return promise
        return db.sequelize.query(`TRUNCATE "Users", "Fields","Stores" RESTART IDENTITY`)
            .then(() => {

                // Return promise
                return User.create({
                    email: 'admin@mail.com',
                    password: 'admin',
                    role: "admin"
                })
            })
            .then(admin => {

                // Assign the token
                token = jwt.sign({
                    id: admin.id,
                    email: admin.email,
                    role: admin.role
                }, process.env.SECRET)
                // console.log("ini token======", token)

            })
            .then(() => {

                // Return promise
                return User.create({
                    email: 'testerlagi@mail.com',
                    password: '12345678',
                    role: "member"
                })

            })
    })

    afterAll(() => {
        return Promise.all([
            db.sequelize.query(`TRUNCATE "Users", "Fields", "Stores" RESTART IDENTITY`),
            User.destroy({
                where: {
                    email: 'admin@mail.com'
                },
                truncate: true,
                cascade: true
            }),
            Field.destroy({
                where: {
                    id: "1"
                }
            }),
            Store.destroy({
                where: {
                    id: "1"
                }
            }),
            User.destroy({
                where: {
                    id: '2'
                },
                truncate: true,
                cascade: true
            })
        ])
    })

    describe('POST /api/v1/admin/register', () => {
        test('Success Admin Register. Status code 200', done => {
            request(app)
                .post('/api/v1/admin/register')
                .set('Content-Type', 'application/json')
                .send({
                    store_name: 'golden goal futsal',
                    email: 'admingolden@mail.com',
                    password: 'admingolde'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('POST /api/v1/admin/login', () => {
        test('Success Admin Login. Status code 200', done => {
            request(app)
                .post('/api/v1/admin/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'admingolden@mail.com',
                    password: 'admingolde'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    // describe('GET /api/v1/store/', () => {
    //     test('Success Show all store registered', done => {
    //         request(app)
    //             .get('/api/v1/store/')
    //             .then(res => {
    //                 expect(res.statusCode).toEqual(200);
    //                 expect(res.body.status).toEqual('success');
    //                 done();
    //             })
    //     })
    // })
    describe('GET /api/v1/store/', () => {
        test('Failed Show all store registered', done => {
            request(app)
                .get('/api/v1/admin/store/')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/store/', () => {
        test('Failed Show all store registered', done => {
            request(app)
                .get('/api/v1/admin/store/')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/store/info', () => {
        test('Failed Show specific store profile', done => {
            request(app)
                .get('/api/v1/admin/store/info')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })


})