const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Field,
    Store
} = require('../models');
const jwt = require('jsonwebtoken');
const admin = require('../controllers/admin');

let token;

describe('Test Field API Collection', () => {

    beforeAll(() => {
        // This whole code will return promise
        return db.sequelize.query(`TRUNCATE "Users", "Fields", "Stores" RESTART IDENTITY`)
            .then(() => {

                // Return promise
                return User.create({
                    email: 'admin@mail.com',
                    password: 'admin',
                    role: "admin"
                })
            })
            .then(admin => {
                // Assign the token
                token = jwt.sign({
                    id: admin.id,
                    email: admin.email,
                    role: admin.role
                }, process.env.SECRET)
                return Store.create({
                    store_name: 'futsal baru',
                    user_id: admin.id
                })
            })

    })

    afterAll(() => {
        return Promise.all([
            db.sequelize.query(`TRUNCATE "Users", "Fields", "Stores" RESTART IDENTITY`),
            User.destroy({
                where: {
                    email: 'admin@mail.com'
                },
                truncate: true,
                cascade: true
            }),
            Field.destroy({
                where: {
                    id: "1"
                }
            }),
            Store.destroy({
                where: {
                    id: "1"
                }
            })
        ])
    })


    describe('PUT /api/v1/field/create', () => {
        test('Success create Field', done => {
            request(app)
                .put('/api/v1/field/create')
                .set('Authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_collection/hideThePain.jpeg')
                .field({
                    name: "lap satu",
                    field_type: "beton",
                    price: "100000",
                    avg_rating: 0
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('PUT /api/v1/field/create', () => {
        test('Failed create Field', done => {
            request(app)
                .put('/api/v1/field/create')
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_collection/hideThePain.jpeg')
                .field({
                    field_type: "beton",
                    price: "100000",
                    avg_rating: 0
                })
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('PUT /api/v1/field/create', () => {
        test('Failed create Field', done => {
            request(app)
                .put('/api/v1/field/create')
                .set('Authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_collection/hideThePain.jpeg')
                .field({
                    field_type: "beton",
                    price: "100000",
                    avg_rating: 0
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('PUT /api/v1/field/update/:id', () => {
        test('Success update Field', done => {
            request(app)
                .put('/api/v1/field/update/1')
                .set('Authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_collection/hideThePain.jpeg')
                .field({
                    name: "lap satu update",
                    address: "jl mana tu man",
                    phone_number: "0877777",
                    field_type: "Vynil",
                    facilities: "makan sepuasnya",
                    price: "150000"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('PUT /api/v1/field/update/:id', () => {
        test('Failed update Field', done => {
            request(app)
                .put('/api/v1/field/update/1')
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_collection/hideThePain.jpeg')
                .field({
                    name: "lap satu update",
                    address: "jl mana tu man",
                    phone_number: "0877777",
                    field_type: "Vynil",
                    facilities: "makan sepuasnya",
                    price: "150000"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/', () => {
        test('Success show Field on specific Page', done => {
            request(app)
                .get('/api/v1/field/?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/', () => {
        test('Failed show Field on specific Page', done => {
            request(app)
                .get('/api/v1/field/')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/priceDesc', () => {
        test('Success show Field sort by price Descending', done => {
            request(app)
                .get('/api/v1/field/priceDesc?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/priceDesc', () => {
        test('Failed show Field sort by price Descending', done => {
            request(app)
                .get('/api/v1/field/priceDesc?pagge=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/priceAsc', () => {
        test('Success show Field sort by price Ascending', done => {
            request(app)
                .get('/api/v1/field/priceAsc?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/priceAsc', () => {
        test('Failed show Field sort by price Ascending', done => {
            request(app)
                .get('/api/v1/field/priceAsc?pagef=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/nameDesc', () => {
        test('Success show Field sort by price Descending', done => {
            request(app)
                .get('/api/v1/field/nameDesc?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/nameDesc', () => {
        test('Failed show Field sort by price Descending', done => {
            request(app)
                .get('/api/v1/field/nameDesc?pagge=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/nameAsc', () => {
        test('Success show Field sort by price Ascending', done => {
            request(app)
                .get('/api/v1/field/nameAsc?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/nameAsc', () => {
        test('Failed show Field sort by price Ascending', done => {
            request(app)
                .get('/api/v1/field/nameAsc?pdage=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/ratingAsc', () => {
        test('Success show Field sort by rating Ascending', done => {
            request(app)
                .get('/api/v1/field/ratingAsc?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/ratingAsc', () => {
        test('Failed show Field sort by rating Ascending', done => {
            request(app)
                .get('/api/v1/field/ratingAsc?paggge=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/ratingDesc', () => {
        test('Success show Field sort by rating Descending', done => {
            request(app)
                .get('/api/v1/field/ratingDesc?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/ratingDesc', () => {
        test('Success show Field sort by rating Descending', done => {
            request(app)
                .get('/api/v1/field/ratingDesc?pfagge=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/type?page=1', () => {
        test('Success show Field filter by type_field', done => {
            request(app)
                .get('/api/v1/field/type?page=1')
                .set('Content-Type', 'query-params')
                .set('Content-Type', 'application/json')
                .send({
                    field_type: 'Vynil'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/type?page=1', () => {
        test('Failed show Field filter by type_field', done => {
            request(app)
                .get('/api/v1/field/type?pagge=1')
                .set('Content-Type', 'query-params')
                .set('Content-Type', 'application/json')
                .send({
                    field_type: 'Vynil'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/search?page=1', () => {
        test('Success search field by name', done => {
            request(app)
                .get('/api/v1/field/search?page=1')
                .set('Content-Type', 'query-params')
                .set('Content-Type', 'application/json')
                .send({
                    store_name: 'futsal'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/search?page=1', () => {
        test('Failed search field by name', done => {
            request(app)
                .get('/api/v1/field/search?pgge=1')
                .set('Content-Type', 'query-params')
                .set('Content-Type', 'application/json')
                .send({
                    store_name: 'futsal'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/one/:id', () => {
        test('Success show specific Field', done => {
            request(app)
                .get('/api/v1/field/one/1')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/one/:id', () => {
        test('Failed show specific Field', done => {
            request(app)
                .get('/api/v1/field/one/b')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/store', () => {
        test('Failed show all Field in one store', done => {
            request(app)
                .get('/api/v1/field/store')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/field/store', () => {
        test('Failed show all Field in one store', done => {
            request(app)
                .get('/api/v1/field/store')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('DELETE /api/v1/field/delete/:id', () => {
        test('Success delete specific Field', done => {
            request(app)
                .delete('/api/v1/field/delete/1')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('DELETE /api/v1/field/delete/:id', () => {
        test('Failed delete specific Field', done => {
            request(app)
                .delete('/api/v1/field/delete/1')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('DELETE /api/v1/field/delete/:id', () => {
        test('Failed delete specific Field', done => {
            request(app)
                .delete('/api/v1/field/delete/2')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(410);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
})
