const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Field,
    Schedule,
    Store
} = require('../models');
const jwt = require('jsonwebtoken');

let token;

describe('Test Schedule API Collection', () => {

    beforeAll(() => {
        // This whole code will return promise
        return db.sequelize.query(`TRUNCATE "Users", "Fields", "Schedules", "Stores" RESTART IDENTITY`)
            .then(() => {
                // Return promise
                return User.create({
                    email: 'admin@mail.com',
                    password: 'admin',
                    role: "admin"
                })

            })
            .then(admin => {
                // Assign the token
                tokenAdmin = jwt.sign({
                    id: admin.id,
                    email: admin.email,
                    role: admin.role
                }, process.env.SECRET)
                return Store.create({
                    store_name: 'futsal baru',
                    user_id: admin.id
                })
            })
    })

    afterAll(() => {
        return Promise.all([
            db.sequelize.query(`TRUNCATE "Users", "Fields", "Schedules", "Stores" RESTART IDENTITY`),
            User.destroy({
                where: {
                    email: 'admin@mail.com'
                },
                truncate: true,
                cascade: true
            }),
            Field.destroy({
                where: {
                    id: "1"
                }
            }),
            Schedule.destroy({
                where: {
                    id: "1"
                }
            }),
            Store.destroy({
                where: {
                    id: "1"
                }
            })
        ])
    })


    describe('PUT /api/v1/field/create', () => {
        test('Success create Field', done => {
            request(app)
                .put('/api/v1/field/create')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_collection/hideThePain.jpeg')
                .field({
                    name: "lap satu",
                    field_type: "beton",
                    price: "100000",
                    avg_rating: 0
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('POST /api/v1/schedule/create/1', () => {
        test('Success create Schedule in specific Field', done => {
            request(app)
                .post('/api/v1/schedule/create/1')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'application/json')
                .send({
                    date: "2020-07-25",
                    schedule_time: "13.00-14.00"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('POST /api/v1/schedule/create/1', () => {
        test('Failed create Schedule in specific Field', done => {
            request(app)
                .post('/api/v1/schedule/create/1')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'application/json')
                .send({
                    dateasd: "2020-07-25",
                    schedule_time: "13.00-14.00"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/schedule/1', () => {
        test('Success show a Schedule in specific Field', done => {
            request(app)
                .get('/api/v1/schedule/1?date=2020-07-25')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/schedule/showall/:id', () => {
        test('Success show all Schedule in specific Field', done => {
            request(app)
                .get('/api/v1/schedule/showall/1')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/schedule/showall/:id', () => {
        test('Failed show all Schedule in specific Field', done => {
            request(app)
                .get('/api/v1/schedule/showall/a')
                .then(res => {
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('PUT /api/v1/schedule/update/available/:id', () => {
        test('Success update availabilty to true', done => {
            request(app)
                .put('/api/v1/schedule/update/available/1')
                .set('Authorization', tokenAdmin)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('PUT /api/v1/schedule/update/available/:id', () => {
        test('Failed update availabilty to true', done => {
            request(app)
                .put('/api/v1/schedule/update/available/1a')
                .set('Authorization', tokenAdmin)
                .then(res => {
                    expect(res.statusCode).toEqual(500);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('PUT /api/v1/schedule/update/unavailable/:id', () => {
        test('Success update availabilty to false', done => {
            request(app)
                .put('/api/v1/schedule/update/unavailable/1')
                .set('Authorization', tokenAdmin)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('PUT /api/v1/schedule/update/unavailable/:id', () => {
        test('Failed update availabilty to false', done => {
            request(app)
                .put('/api/v1/schedule/update/unavailable/1v')
                .set('Authorization', tokenAdmin)
                .then(res => {
                    expect(res.statusCode).toEqual(500);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('DELETE /api/v1/schedule/delete/1', () => {
        test('Success delete schedule', done => {
            request(app)
                .delete('/api/v1/schedule/delete/1')
                .set('Authorization', tokenAdmin)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('DELETE /api/v1/schedule/delete/1', () => {
        test('Failed delete schedule', done => {
            request(app)
                .delete('/api/v1/schedule/delete/1a')
                .set('Authorization', tokenAdmin)
                .then(res => {
                    expect(res.statusCode).toEqual(500);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })





})
