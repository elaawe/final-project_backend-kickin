const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    Profile,
    User,
    Field,
    Review,
    Store
} = require('../models');
const jwt = require('jsonwebtoken');

let token;
let token1

describe('Tes Review API Collection', () => {
    beforeAll(() => {
        // This whole code will return promise
        return db.sequelize.query(`TRUNCATE "Users", "Fields", "Reviews", "Stores" RESTART IDENTITY`)
            .then(() => {
                // Return promise
                return User.create({
                    email: 'admin@mail.com',
                    password: 'admin',
                    role: "admin"
                })
            })
            .then(admin => {

                // Assign the token
                tokenAdmin = jwt.sign({
                    id: admin.id,
                    email: admin.email,
                    role: admin.role
                }, process.env.SECRET)
                return Store.create({
                    store_name: 'futsal baru',
                    user_id: admin.id
                })
            })
            .then(() => {
                // Return promise
                return User.create({
                    email: 'asyharifadli@mail.com',
                    password: 'ojolaliyo1',
                    name: "Fadli"
                })

            })
            .then(user => {

                // Assign the token
                token = jwt.sign({
                    id: user.id,
                    email: user.email,
                    role: user.role
                }, process.env.SECRET)
            })
    })
    afterAll(() => {
        return Promise.all([
            db.sequelize.query(`TRUNCATE "Users", "Fields", "Reviews", "Stores" RESTART IDENTITY`),
            User.destroy({
                where: {
                    email: 'admin@mail.com' && 'asyharifadli@mail.com'
                },
                truncate: true,
                cascade: true
            }),
            Field.destroy({
                where: {
                    id: "1"
                }
            }),
            Review.destroy({
                where: {
                    id: "1"
                }
            }),
            Store.destroy({
                where: {
                    id: "1"
                }
            }),
        ])
    })
    
    describe('PUT /api/v1/field/create', () => {
        test('Success create Field', done => {
            request(app)
                .put('/api/v1/field/create')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_collection/hideThePain.jpeg')
                .field({
                    name: "lap satu",
                    field_type: "beton",
                    price: "100000",
                    avg_rating: 0
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('POST /api/v1/review/create/1', () => {
        test('Success create Review in specific Field', done => {
            request(app)
                .post('/api/v1/review/create/1')
                .set('Authorization', token)
                .set('Content-Type', 'application/json')
                .send({
                    comment: "lapangane bosok petugase angel kandanane",
                    rating: "2"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('POST /api/v1/review/create/1', () => {
        test('Failed create Review in specific Field', done => {
            request(app)
                .post('/api/v1/review/create/1')
                .set('Authorization', token)
                .set('Content-Type', 'application/json')
                .send({
                    comment: "lapangane bosok petugase angel kandanane",
                    rating: "2"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/review/all/1?page=1', () => {
        test('Success find all review at page 1 in specific Field', done => {
            request(app)
                .get('/api/v1/review/all/1?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/review/rating/1', () => {
        test('Success show overal rating in specific Field', done => {
            request(app)
                .get('/api/v1/review/rating/1')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/review/user?page=1', () => {
        test('Success show all review from specific User', done => {
            request(app)
                .get('/api/v1/review/user?page=1')
                .set('Content-Type', 'query-params')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/review/all/1?page=1', () => {
        test('Success show all review in specific Field', done => {
            request(app)
                .get('/api/v1/review/all/1?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('PUT /api/v1/review/update/1', () => {
        test('Success update a review in specific Field', done => {
            request(app)
                .put('/api/v1/review/update/1')
                .set('Authorization', token)
                .set('Content-Type', 'application/json')
                .send({
                    comment: "hehe becanda lapangannya mantab kok",
                    rating: "5"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('DELETE /api/v1/review/delete/1', () => {
        test('Success delete a review in specific Field', done => {
            request(app)
                .delete('/api/v1/review/delete/1')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
})
