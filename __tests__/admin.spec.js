const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    User,
    Field,
    Store
} = require('../models');
const jwt = require('jsonwebtoken');

let token;

describe('Test Field API Collection', () => {

    beforeAll(() => {
        // This whole code will return promise
        return db.sequelize.query(`TRUNCATE "Users", "Fields" RESTART IDENTITY`)
            .then(() => {

                // Return promise
                return User.create({
                    email: 'admin@mail.com',
                    password: 'admin',
                    role: "admin"
                })
            })
            .then(admin => {

                // Assign the token
                token = jwt.sign({
                    id: admin.id,
                    email: admin.email,
                    role: admin.role
                }, process.env.SECRET)
                // console.log("ini token======", token)

            })
            .then(() => {

                // Return promise
                return User.create({
                    email: 'testerlagi@mail.com',
                    password: '12345678',
                    role: "member"
                })

            })
    })

    afterAll(() => {
        return Promise.all([
            db.sequelize.query(`TRUNCATE "Users", "Fields", Stores RESTART IDENTITY`),
            User.destroy({
                where: {
                    email: 'admin@mail.com'
                },
                truncate: true,
                cascade: true
            }),
            Field.destroy({
                where: {
                    id: "1"
                }
            }),
            Store.destroy({
                where: {
                    id: "1"
                }
            }),
            User.destroy({
                where: {
                    id: '2'
                },
                truncate: true,
                cascade: true
            })
        ])
    })

    describe('POST /api/v1/admin/register', () => {
        test('Success Admin Register. Status code 200', done => {
            request(app)
                .post('/api/v1/admin/register')
                .set('Content-Type', 'application/json')
                .send({
                    store_name: 'golden goal futsal',
                    email: 'admingolden@mail.com',
                    password: 'admingolde'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('POST /api/v1/admin/register', () => {
        test('Failed Admin Register. because of store_name already exist', done => {
            request(app)
                .post('/api/v1/admin/register')
                .set('Content-Type', 'application/json')
                .send({
                    store_name: 'golden goal futsal',
                    email: 'admingolde@mail.com',
                    password: 'admingolde'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(403);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('POST /api/v1/admin/register', () => {
        test('Failed Admin Register. because of email already exist', done => {
            request(app)
                .post('/api/v1/admin/register')
                .set('Content-Type', 'application/json')
                .send({
                    store_name: 'mantab futsal',
                    email: 'admingolden@mail.com',
                    password: 'admingolde'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(403);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('POST /api/v1/admin/register', () => {
        test('Failed Admin Register. because of password too long', done => {
            request(app)
                .post('/api/v1/admin/register')
                .set('Content-Type', 'application/json')
                .send({
                    store_name: 'mantab futsal',
                    email: 'adminmantab@mail.com',
                    password: 'adasdasdsaqew1231239129389ehihsuhdkhsakjdhjashdjhasuiuy732y8'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('POST /api/v1/admin/login', () => {
        test('Success Admin Login. Status code 200', done => {
            request(app)
                .post('/api/v1/admin/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'admin@mail.com',
                    password: 'admin'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('POST /api/v1/admin/login', () => {
        test('Failed Admin Login. password incorrect', done => {
            request(app)
                .post('/api/v1/admin/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'admin@mail.com',
                    password: 'admina'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('POST /api/v1/admin/login', () => {
        test('Failed Admin Login. email doesnt exist', done => {
            request(app)
                .post('/api/v1/admin/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'adminaa@mail.com',
                    password: 'admin'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(403);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('PUT /api/v1/admin/update/2', () => {
        test('Success update specific User', done => {
            request(app)
                .put('/api/v1/admin/update/2')
                .set('Authorization', token)
                .set('Content-Type', 'application/json')
                .send({
                    email: 'testerlag@mail.com'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('GET /api/v1/admin/findUser/2', () => {
        test('Success find specific User', done => {
            request(app)
                .get('/api/v1/admin/findUser/2')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('GET /api/v1/admin/findAllUser', () => {
        test('Success find All User', done => {
            request(app)
                .get('/api/v1/admin/findAllUser?page=1')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })



    describe('POST /api/v1/admin/searchUser?page=1', () => {
        test('Success search specific User by email', done => {
            request(app)
                .post('/api/v1/admin/searchUser?page=1')
                .set('Authorization', token)
                .set('Content-Type', 'application/json')
                .send({
                    email: 'testerlagi'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('DELETE /api/v1/admin/delete/2', () => {
        test('Success delete specific User', done => {
            request(app)
                .put('/api/v1/admin/delete/2')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
})