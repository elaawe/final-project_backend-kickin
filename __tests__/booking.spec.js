const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
    Profile,
    User,
    Field,
    Schedule,
    Store,
    Payment,
    Booking

} = require('../models');
const jwt = require('jsonwebtoken');
const admin = require('../controllers/admin');
const { adminDelete } = require('../controllers/admin');

let token;
let tokenAdmin;
let bookingCode;

describe('Test Booking API Collection', () => {

    beforeAll(() => {
        // This whole code will return promise
        return db.sequelize.query(`TRUNCATE "Users","Profiles", "Fields", "Schedules", "Stores", "Bookings", "Payments" RESTART IDENTITY`)
            .then(() => {

                // Return promise
                return User.create({
                    email: 'admin@mail.com',
                    password: 'admin',
                    role: "admin"
                })
            })
            .then(admin => {
                // Assign the token
                tokenAdmin = jwt.sign({
                    id: admin.id,
                    email: admin.email,
                    role: admin.role
                }, process.env.SECRET)
                return Store.create({
                    store_name: 'futsal baru',
                    user_id: admin.id
                })
            })
            .then(() => {
                return User.create({
                    email: 'testerlagi@mail.com',
                    password: '12345678',
                    role: "member"
                })
            })
            .then(user => {
                token = jwt.sign({
                    id: user.id,
                    email: user.email,
                    role: user.role
                }, process.env.SECRET)
                return Profile.create({
                    user_id: user.id,
                    name: "abdul"
                  })
            })
    })

    afterAll(() => {
        return Promise.all([
            db.sequelize.query(`TRUNCATE "Users","Profiles", "Fields", "Schedules", "Stores", "Bookings", "Payments" RESTART IDENTITY`),
            User.destroy({
                where: {
                    email: 'admin@mail.com' && 'testerlagi@mail.com'
                },
                truncate: true,
                cascade: true
            }),
            Field.destroy({
                where: {
                    id: "1"
                },
                truncate: true,
                cascade: true
            }),
            Schedule.destroy({
                where: {
                    id: "1"
                },
                truncate: true,
                cascade: true
            }),
            Booking.destroy({
                where: {
                    id: "1"
                },
                truncate: true,
                cascade: true
            }),
            Payment.destroy({
                where: {
                    id: "1"
                },
                truncate: true,
                cascade: true
            }),
            Profile.destroy({
                where: {
                    id: "1"
                },
                truncate: true,
                cascade: true
            })
        ])
    })

    describe('PUT /api/v1/field/create', () => {
        test('Success create Field', done => {
            request(app)
                .put('/api/v1/field/create')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_collection/hideThePain.jpeg')
                .field({
                    name: "lap satu",
                    field_type: "beton",
                    price: "100000",
                    avg_rating: 0
                })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('POST /api/v1/schedule/create/1', () => {
        test('Success create Schedule in specific Field', done => {
            request(app)
                .post('/api/v1/schedule/create/1')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'application/json')
                .send({
                    date: "2020-07-25",
                    schedule_time: "13.00-14.00"
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('PUT /api/v1/booking/create/1', () => {
        test('Success create Booking in specific Field', done => {
            request(app)
                .put('/api/v1/booking/create/1')
                .set('Authorization', token)
                .set('Content-Type', 'application/json')
                .send({
                    date: "2020-07-25",
                    schedule_time: "13.00-14.00"
                })
                .then(res => {
                    bookingCode = res.body.data.booking_code
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/checkout', () => {
        test('Success show all booking cart', done => {
            request(app)
                .get('/api/v1/booking/checkout?page=1')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/checkout', () => {
        test('Failed show all booking cart', done => {
            request(app)
                .get('/api/v1/booking/checkout?pagge=1')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/checkout', () => {
        test('Failed show all booking cart', done => {
            request(app)
                .get('/api/v1/booking/checkout?page=1')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/checkoutrn', () => {
        test('Success show all booking cart', done => {
            request(app)
                .get('/api/v1/booking/checkoutrn')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/checkout/1', () => {
        test('Success show specific booking checkout', done => {
            request(app)
                .get('/api/v1/booking/checkout?page=1')
                .set('Authorization', token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    // describe('POST /api/v1/booking/pay/1', () => {
    //     test('Success upload proof_of_payment for specific booking', done => {
    //         request(app)
    //             .post('/api/v1/booking/pay/1')
    //             .set('Authorization', token)
    //             .set('Content-Type', 'multipart/form-data')
    //             .attach('image', './lib/image_collection/hideThePain.jpeg')
    //             .then(res => {
    //                 expect(res.statusCode).toEqual(201);
    //                 expect(res.body.status).toEqual('success');
    //                 done();
    //             })
    //     })
    // })

    describe('GET /api/v1/booking/payment?page=1', () => {
        test('Success for store admin to show all payment for his store', done => {
            request(app)
                .get('/api/v1/booking/payment?page=1')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/payment?page=1', () => {
        test('Failed for store admin to show all payment for his store', done => {
            request(app)
                .get('/api/v1/booking/payment?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/payment?page=1', () => {
        test('Failed for store admin to show all payment for his store', done => {
            request(app)
                .get('/api/v1/booking/payment?pagae=1')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('PUT /api/v1/booking/verify', () => {
        test('Success for store admin to verify a payment for his store', done => {
            request(app)
                .put('/api/v1/booking/verify')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'application/json')
                .send({
                    booking_code: bookingCode
                })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('PUT /api/v1/booking/verify', () => {
        test('Failed for store admin to verify a payment for his store', done => {
            request(app)
                .put('/api/v1/booking/verify')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'application/json')
                .send({
                    booking_code: '0000120070000'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('PUT /api/v1/booking/verify', () => {
        test('Failed for store admin to verify a payment for his store', done => {
            request(app)
                .put('/api/v1/booking/verify')
                .set('Content-Type', 'application/json')
                .send({
                    booking_code: '00001200700001'
                })
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/showall?page=1', () => {
        test('Success show all booking from 1 store', done => {
            request(app)
                .get('/api/v1/booking/showall?page=1')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/showall?page=1', () => {
        test('Failed show all booking from 1 store', done => {
            request(app)
                .get('/api/v1/booking/showall?pfage=1')
                .set('Authorization', tokenAdmin)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/showall?page=1', () => {
        test('Failed show all booking from 1 store', done => {
            request(app)
                .get('/api/v1/booking/showall?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('GET /api/v1/booking/tickets?page=1', () => {
        test('Success show user booking tickets', done => {
            request(app)
                .get('/api/v1/booking/tickets?page=1')
                .set('Authorization', token)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/tickets?page=1', () => {
        test('Failed show user booking tickets', done => {
            request(app)
                .get('/api/v1/booking/tickets?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/tickets?page=1', () => {
        test('Failed show user booking tickets', done => {
            request(app)
                .get('/api/v1/booking/tickets?psage=1')
                .set('Authorization', token)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/history?page=1', () => {
        test('Success show user booking history', done => {
            request(app)
                .get('/api/v1/booking/history?page=1')
                .set('Authorization', token)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/history?page=1', () => {
        test('Failed show user booking history', done => {
            request(app)
                .get('/api/v1/booking/history?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/history?page=1', () => {
        test('Failed show user booking history', done => {
            request(app)
                .get('/api/v1/booking/history?pdage=1')
                .set('Authorization', token)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/list?page=1', () => {
        test('Success show user booking list', done => {
            request(app)
                .get('/api/v1/booking/list?page=1')
                .set('Authorization', token)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/list?page=1', () => {
        test('Failed show user booking list', done => {
            request(app)
                .get('/api/v1/booking/list?pa2e=1')
                .set('Authorization', token)
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('GET /api/v1/booking/list?page=1', () => {
        test('Failed show user booking list', done => {
            request(app)
                .get('/api/v1/booking/list?page=1')
                .set('Content-Type', 'query-params')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('DELETE /api/v1/booking/delete/1', () => {
        test('Failed for store admin to show all payment for his store', done => {
            request(app)
                .delete('/api/v1/booking/delete/2')
                .set('Authorization', tokenAdmin)
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    describe('DELETE /api/v1/booking/delete/1', () => {
        test('Success for store admin to show all payment for his store', done => {
            request(app)
                .delete('/api/v1/booking/delete/1')
                .set('Authorization', tokenAdmin)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    describe('DELETE /api/v1/booking/delete/1', () => {
        test('Failed for store admin to show all payment for his store', done => {
            request(app)
                .delete('/api/v1/booking/delete/1')
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
})