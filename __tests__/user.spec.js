const request = require('supertest');
const app = require('../app');
const db = require('../models');
const {
  Profile,
  Image,
  User
} = require('../models');
const jwt = require('jsonwebtoken');

let token;

describe('Test User API Collection', () => {

  beforeAll(() => {
    // This whole code will return promise
    return db.sequelize.query(`TRUNCATE "Users", "Profiles" RESTART IDENTITY`)
      .then(() => {
        // Return promise
        return User.create({
          email: 'testerlagi@mail.com',
          password: '12345678',
          role: "member"
        })
      })
      .then(user => {
        // Assign the token
        token = jwt.sign({
          id: user.id,
          email: user.email,
          role: user.role
        }, process.env.SECRET)
        // Return the Promise
        return Profile.create({
          user_id: user.id,
          name: "abdul",
          age: "22",
          avatar: "https://akcdn.detik.net.id/visual/2019/07/11/cb8f1cf4-92bb-4df1-8460-f0fbdfce01d4_169.jpeg?w=650",
          motto: "hiduplah seperti larry",
          favorite_position: "LWF",
          favorite_team: "Liverfool"
        })
      })
  })

  afterAll(() => {
    return Promise.all([
      db.sequelize.query(`TRUNCATE "Users", "Profiles" RESTART IDENTITY`),
      User.destroy({
        where: {
          email: 'tester@email.com' && 'testerlagi@email.com'
        }
      })
    ])
  })
  describe('GET /api/v1/user/check', () => {
    test('Success checking the Token. Status code 200', done => {
      request(app)
        .get('/api/v1/user/check')
        .set('Content-Type', 'application/json')
        .set('Authorization', token)
        .then(res => {
          expect(res.statusCode).toEqual(200);
          expect(res.body.status).toEqual('success');
          done();
        })
    })
  })
  describe('POST /api/v1/user/register', () => {
    test('Success register. Status code 201', done => {
      request(app)
        .post('/api/v1/user/register')
        .set('Content-Type', 'application/json')
        .send({
          email: 'tester@email.com',
          password: '12345asas',
          name: 'abdul'
        })
        .then(res => {
          expect(res.statusCode).toEqual(201);
          expect(res.body.status).toEqual('success');
          done();
        })
    })
    test('Failed register. Status code 422', done => {
      request(app)
        .post('/api/v1/user/register')
        .set('Content-Type', 'application/json')
        .send({
          email: 'Tester@email.com',
          password: '1',
          name: ""
        })
        .then(res => {
          expect(res.statusCode).toEqual(422);
          expect(res.body.status).toEqual('fail');
          done();
        })
    })
  })


  describe('POST /api/v1/user/login', () => {
    test('Success Login. Status code 200', done => {
      request(app)
        .post('/api/v1/user/login')
        .set('Content-Type', 'application/json')
        .send({
          email: 'tester@email.com',
          password: '12345asas'
        })
        .then(res => {
          expect(res.statusCode).toEqual(200);
          expect(res.body.status).toEqual('success');
          done();
        })
    })
    test('Failed Login. Status code 401', done => {
      request(app)
        .post('/api/v1/user/login')
        .set('Content-Type', 'application/json')
        .send({
          email: 'tester@email.com',
          password: '12345aas'
        })
        .then(res => {
          expect(res.statusCode).toEqual(401);
          expect(res.body.status).toEqual('fail');
          done();
        })
    })
  })

  describe('PUT /api/v1/profile/', () => {
    test('Success updated profile', done => {
      request(app)
        .put('/api/v1/profile/')
        .set('Authorization', token)
        .set('Content-Type', 'multipart/form-data')
        .attach('image', './lib/image_collection/hideThePain.jpeg')
        .field({
          name: "JohnDoe",
          motto: "Lorem Ipsumsadsadsadasdasdsadsad",
          age: "22",
          favorite_team: "MU",
          favorite_position: "LMF"
        })
        .then(res => {
          expect(res.statusCode).toEqual(201);
          expect(res.body.status).toEqual('success');
          done();
        })
    })
    test('failed updated profile because token is invalid', done => {
      request(app)
        .put('/api/v1/profile/')
        .set('Authorization', { token: "12798124yhekdhawkdhkjashdasdsjahduy21" })
        .set('Content-Type', 'multipart/form-data')
        .attach('image', './lib/image_collection/hideThePain.jpeg')
        .field({
          name: "Nametest",
          motto: "teasdasdasdasdsadsaasadada",
          age: "22",
          favorite_team: "MU",
          favorite_position: "LMF"
        })
        .then(res => {
          expect(res.statusCode).toEqual(401);
          expect(res.body.status).toEqual('fail');
          done();
        })
    })
  })

})
