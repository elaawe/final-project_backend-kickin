# KICKIN APP API
A backend API for Futsal Booking and field management Web application, we called it [KICKIN](https://kickinapp.herokuapp.com/)

## Tech 
-   Nodejs
-   ExpressJs
-   PostgreSql
-   Sequelize (ORM)
-   Heroku
-   Google Oauth2
-   SendGrid
-   Jest & Supertest

# Feature
## Admin
-   Login
-   Register
-   Update Profile
-   create field
-   update field
-   delete field
-   add Field Schedule
-   Approve Payment Booking

## User
-   Login
-   Register
-   Update Profile
-   search, filter Field
-   Booking Field
-   Payment Upload
-   Review Field
