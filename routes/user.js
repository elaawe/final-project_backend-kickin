const router = require('express').Router()
const user = require('../controllers/user')
const success = require('../middlewares/success')
const authentication = require('../middlewares/authentication')

router.post('/register', user.register)
router.post('/login', user.login)
router.post('/googleLogin', user.googleLogin)

router.get('/check', authentication, user.check)

module.exports = router