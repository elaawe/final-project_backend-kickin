const router = require('express').Router()
const schedule = require('../controllers/schedule')
const authentication = require('../middlewares/authentication')
const isAdmin = require('../middlewares/isAdmin')
const success = require('../middlewares/success')

router.get('/:id', schedule.showSchedules, success)
router.get('/showall/:id/', schedule.showAllSchedules, success)

router.use(authentication)
router.use(isAdmin)
router.post('/create/:id', schedule.create, success)
router.delete('/delete/:id', schedule.deleteSchedule, success)
router.put('/update/available/:id', schedule.updateSchedulesAvailable, success)
router.put('/update/unavailable/:id', schedule.updateSchedulesUnavailable, success)

module.exports = router
