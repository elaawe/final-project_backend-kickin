const router = require('express').Router()
const store = require('../controllers/store')
const authentication = require('../middlewares/authentication')
const isAdmin = require('../middlewares/isAdmin')
const uploader = require('../middlewares/uploader');

// uploader.single('image'),
router.get('/', store.getAllStore)

router.use(authentication)
router.get('/info', store.getStoreInfo)
router.put('/update', isAdmin, uploader.single('image'), store.update)
router.delete('/delete/:id', store.deleteStore)
module.exports = router
