const router = require('express').Router()
const admin = require('../controllers/admin')
const success = require('../middlewares/success')
const authentication = require('../middlewares/authentication')
const isAdmin = require('../middlewares/isAdmin')
// const uploader = require('../middlewares/uploader');


router.post('/register', admin.register, success)
router.post('/login', admin.adminLogin, success)

router.use(authentication)
router.use(isAdmin)
router.get('/findUser/:id', admin.adminFindOne, success)
router.post(`/searchUser`, admin.adminSearchUser, success)
router.get('/findAllUser', admin.adminFindAll, success)
router.put('/update/:id', admin.adminUpdate, success)
router.put('/delete/:id', admin.adminDelete, success)

module.exports = router