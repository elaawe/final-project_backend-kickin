const router = require('express').Router()
const user = require('./user')
const admin = require('./admin')
const profile = require('./profile')
const field = require('./field')
const booking = require('./booking')
const review = require('./review')
const schedule = require('./schedule')
const store = require('./store')
const image_field = require('./image_field')

router.use('/user', user)
router.use('/admin', admin)
router.use('/schedule', schedule)
router.use('/profile', profile)
router.use('/booking', booking)
router.use('/field', field)
router.use('/review', review)
router.use('/schedule', schedule)
router.use('/store', store)
router.use('/image-field', image_field)

module.exports = router

