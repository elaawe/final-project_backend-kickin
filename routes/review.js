const router = require('express').Router()
const review = require('../controllers/review')
const authentication = require('../middlewares/authentication')
const authorization = require('../middlewares/authorizationReview')
const isSuperAdmin = require('../middlewares/isSuperAdmin')
const success = require('../middlewares/success')

router.get('/all/:id', review.findAllReview, success)
router.get('/rating/:id', review.showRatingOverall, success)

router.use(authentication)
router.post('/create/:id', review.createReview, success)
router.delete('/delete/:id', authorization, review.deleteReview, success)
router.put('/update/:id',authorization, review.updateReview, success)
router.get('/user', review.findUserReview, success)

router.use(isSuperAdmin)
router.delete('/admin/delete/:id', review.deleteReview, success)

module.exports = router