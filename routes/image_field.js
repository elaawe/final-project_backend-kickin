const router = require('express').Router()
const image_field = require('../controllers/image_field')
const authentication = require('../middlewares/authentication')
const isAdmin = require('../middlewares/isAdmin')
const success = require('../middlewares/success')

router.use(authentication)
router.use(isAdmin)
router.delete('/delete', image_field.deleteImage, success)

module.exports = router
