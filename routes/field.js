const router = require('express').Router()
const field = require('../controllers/field')
const authentication = require('../middlewares/authentication')
const isAdmin = require('../middlewares/isAdmin')
const success = require('../middlewares/success')
const uploader = require('../middlewares/uploader');


router.get('/', field.showField, success)
router.get('/search', field.searchField, success)
router.get('/one/:id', field.showOneField, success)
// Sort Field
router.get('/priceAsc', field.showFieldSortbyPriceAsc, success)
router.get('/priceDesc', field.showFieldSortbyPriceDesc, success)
router.get('/nameAsc', field.showFieldSortbyNameAsc, success)
router.get('/nameDesc', field.showFieldSortbyNameDesc, success)
router.get('/ratingAsc', field.sortByRatingASC, success)
router.get('/ratingDesc', field.sortByRatingDESC, success)
router.get('/type', field.showFiledbyTypeField, success)

router.use(authentication)
router.delete('/delete/:id', isAdmin, field.deleteField, success)
router.put('/create', uploader.array('image', 10), field.createField)
router.put('/update/:id', uploader.array('image', 10), isAdmin, field.update)

router.use(isAdmin)
router.get('/store', field.showFieldbyStore, success)

module.exports = router