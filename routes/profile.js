const router = require('express').Router()
const profile = require('../controllers/profile')
const authentication = require('../middlewares/authentication')
const uploader = require('../middlewares/uploader');
const success = require('../middlewares/success')

router.use(authentication)
router.get('/show', profile.showProfile, success)   
router.put('/', uploader.single('image'), profile.update, success)

module.exports = router