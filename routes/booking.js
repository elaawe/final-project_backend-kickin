const router = require('express').Router()
const booking = require('../controllers/booking')
const authentication = require('../middlewares/authentication')
const isAdmin = require('../middlewares/isAdmin')
const uploader = require('../middlewares/uploader');
const success = require('../middlewares/success')

router.use(authentication)
router.put('/create/:id', booking.createBooking, success)
router.get('/checkout', booking.showAllCheckoutBooking, success)
router.get('/checkoutrn', booking.showAllCheckoutBookingRN, success)
router.get('/checkout/:id', booking.showCheckoutBooking, success)
router.post('/pay/:id', uploader.single('image'), booking.payBooking,  success)
router.get('/tickets', booking.showBookingTicket, success)
router.get('/history', booking.showExpiredBooking, success)
router.get('/list', booking.showBookingList, success)

router.use(isAdmin)
router.get('/showall', booking.showAllBooking, success)
router.delete('/delete/:id', booking.cancelBooking, success)
router.get('/payment', booking.showBookingTransfer, success)
router.put('/verify', booking.verifyBooking, success)
router.delete('/payment/delete/:id', booking.deletePayment, success)

module.exports = router