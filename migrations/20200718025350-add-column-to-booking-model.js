'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Bookings', 'schedule_date', {
      type: Sequelize.STRING,
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Bookings', 'schedule_date')
  }
};
