'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Bookings', 'schedule_time', {
      type: Sequelize.STRING,
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Bookings', 'schedule_time')
  }
};
