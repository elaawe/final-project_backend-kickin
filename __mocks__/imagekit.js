const ImageKit = jest.genMockFromModule('imagekit');
const faker = require('faker');

ImageKit.prototype.upload = () => {
    return Promise.resolve({
        url: faker.image.imageUrl()
    })
}

module.exports = ImageKit;
